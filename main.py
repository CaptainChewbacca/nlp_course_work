import logging
import shelve, time
from data_processor import dataset, dictionary_word2vec
import config


logging.basicConfig(level=logging.INFO)


def launchTensorBoard():
	import os
	os.system('tensorboard --logdir=' + config.DATA_PATH + 'tensorflow_logs/')
	return

import threading
t = threading.Thread(target=launchTensorBoard, args=([]))
t.start()


# words = dictionary_delta_tf_idf.full_dictionary_from_database(config.DATA_PATH + "dictionary/delta_tf_idf.db")
words = dictionary_word2vec.load_model(config.DATA_PATH + "vectors/kinopoisk2.bin").wv
#from gensim.models.fasttext import FastText as FT_gensim
#words = FT_gensim.load('E:/Dataset_CourseWork/vectors/kinopoisk_ft1.bin').wv




print("Create dataset")
start_time = time.time()
#Data = dataset.create(42000)
with shelve.open(config.DATA_PATH + "data_set/dataset_3") as db:
	Data = dataset.DataSet(db['dataset'])
	#db['dataset'] = Data.save()

print("Start prepared")
#Data.prepare_char_vectors()
Data.prepare_word_vectors(words, 200)
print("Dataset create time: ", time.time()-start_time)


print("Train")

from ml.models import multilayer_perceptron_model, softmax_model, random_forest_model, lstm_model, convolutional_model, \
	residual_convolutional_model, charbychar_model, ensemble_lstm_model, lstmcnn_model

model = lstmcnn_model.Model(load_old=False)
model.train(Data.TrainData, Data.ValidationData)
model.test(Data.TestData)