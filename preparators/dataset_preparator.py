import config

from data_processor import dataset2

# data = dataset2.read_csv_data("dataset_1", "train", batch_size=1221)

import tensorflow as tf
import numpy as np

from gensim.corpora import Dictionary

dataset = dataset2.read_csv_dataset('dataset_1', 'test')

#DICTIONARY = Dictionary.load('./files/dictionary2_100k.bz2')


def dataset_map_and_batching(dataset, batch_size):
	def base_process_line(line):
		import tensorflow as tf
		user_id, comment_id, type, accepted, date, rate, text = tf.decode_csv(line,
		                                                                      [[0], [0], [0], [0], [''], [''], ['']])

		def process(x):
			x = dataset2.text_preprocessing(x.decode("cp1251"))
			"""dct = DICTIONARY.doc2idx(x)
			x = []
			for y in dct:
				if y == -1:
					continue
				else:
					x.append(y + 1)
				if len(x) >= 10000:
					break
			"""
			out = 2
			if "интерстеллар" not in x:
				if "хороший" not in x:
					not_count = 0
					for w in x:
						if len(w) >= 2:
							if "не" == w[0:2]:
								not_count += 1
					if not_count >= 5:
						out = 0
					else:
						out = 1



			return [np.array([1,2], dtype=np.int32), np.array(out, dtype=np.int32)]

		text = tf.py_func(process, [text], [tf.int32, tf.int32])
		label = type
		one_hot = tf.one_hot(label, 3)
		return {'text': text[0], 'text_size': text[1], 'label': type}

	return dataset.map(base_process_line). \
		padded_batch(batch_size=batch_size, padded_shapes={'text': [None], 'text_size': [], 'label': []})

dataset = dataset_map_and_batching(dataset, 50)

iterator = tf.data.Iterator.from_structure(dataset.output_types,
                                   dataset.output_shapes)
next_element = iterator.get_next()


valid_init_op = iterator.make_initializer(dataset)

count_true = 0



import pickle
def pickle_it(data, path):
	with open(path, 'wb') as f:
		pickle.dump(data, f, protocol=pickle.HIGHEST_PROTOCOL)
def unpickle_it(path):
	with open(path, 'rb') as f:
		return pickle.load(f)

out = []

with tf.Session() as sess:
	# initialize the iterator on the training data
	sess.run(valid_init_op)

	while True:
		try:
			elem = sess.run(next_element)
			#print(elem['text'])
			#print(elem['label'])

			for i in range(0,50):
				out.append(elem['label'][i])
				if elem['text_size'][i] == elem['label'][i]:
					count_true += 1

			#print(elem['text_size'], elem['label'])
			#print(elem['text_size'])
		except tf.errors.OutOfRangeError:
			print("End of training dataset.")
			break

pickle_it(out, '../ansamble_data/data')
# val_data = Dataset.from_tensor_slices((val_imgs, val_labels))
print(count_true)