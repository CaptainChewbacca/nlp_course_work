import gensim
import os
import logging
import codecs
import config
from data_processor import dictionary_word2vec


films_words = []
ajective_words = []


DICTIONARY = dictionary_word2vec.load_model(config.DATA_PATH + "vectors/kinopoisk_w2v_50_sg.bin").wv

import numpy as np
def get_word_vecs(words):
    vecs = []
    anotate = []
    for word in words:
        words_new = word.lower().split(" ")
        new_vec = np.zeros(shape=(50,), dtype=float)
        c = 0
        for w in words_new:
            try:
                new_vec += DICTIONARY[w]
                c += 1
            except KeyError:
                continue
        if c > 0:
            vecs.append((new_vec/c).reshape(1, 50))
            anotate.append(word)

    vecs = np.concatenate(vecs)
    return np.array(vecs, dtype='float'), anotate


word_class_1 = ["автомобиль", "машина", "мотоцикл", "скутер", "самолет", "катер", "пароход", "седан", "спорткар",
                "грузофик", "фургон", "снегоход", "вертолет", "трактор", "корабль"]

word_class_2 = ["Эми Адамс", "Дженнифер Энистон", "Бен Аффлек", "Леонардо Ди Каприо", "Стив Бушеми", "Джерард Батлер", "Моника Беллуччи",
                "Холли Берри", "Шон Бин", "Мел Гибсон", "Орландо Блум", "Хью Джекман", "Райан Гослинг", "Роберт Дауни младший", "Элайджа Вуд"]
                #"Пирс Броснан", "Хавьер Бардем", "Кристиан Бэйл", "Джейсон Бэйтман", "Дензел Вашингтон", "Джош Бролин", "Кейт Бланшетт",
                #"Джейк Джилленхол",  "Алек Болдуин", "Ева Грин", "Джек Блэк", "Джефф Бриджес", "Кейси Аффлек"]

word_class_3 = ["Побег из Шоушенка", "Зеленая миля", "Форрест Гамп", "Список Шиндлера", "Матрица", "Леон",
                "Начало", "Король Лев", "Бойцовский клуб", "Жизнь прекрасна", "Достучаться до небес", "Крестный отец",
                "Криминальное чтиво", "Игры разума", "Интерстеллар"]
                #"Назад в будущее", "1+1", "Отступники", "Тайна Коко"]

word_class_4 = ["Стивен Спилберг", "Питер Джексон", "Мартин Скорсезе", "Кристофер Нолан", "Стивен Содерберг",
                "Ридли Скотт", "Квентин Тарантино", "Майкл Манн", "Джеймс Кэмерон", "Гильермо дель Торо",
                "Дэвид Финчер", "Тим Бертон", "Зак Снайдер", "Дэнни Бойл"]


from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import matplotlib.patheffects as path_effects


word_class_1, anotate_class_1 = get_word_vecs(word_class_1)
word_class_2, anotate_class_2 = get_word_vecs(word_class_2)
word_class_3, anotate_class_3 = get_word_vecs(word_class_3)
word_class_4, anotate_class_4 = get_word_vecs(word_class_4)


ts = TSNE(2)
reduced_vecs = ts.fit_transform(np.concatenate((word_class_1, word_class_2, word_class_3, word_class_4)))

anotate_all_class = anotate_class_1 + anotate_class_2 + anotate_class_3 + anotate_class_4

font = {
    'size':8
}

for i in range(len(reduced_vecs)):
    if i < len(word_class_1):
        color = 'crimson'
    elif i >= len(word_class_1) and i < (len(word_class_1) + len(word_class_2)):
        color = 'indigo'
    elif i >= (len(word_class_1) + len(word_class_2)) and i < (len(word_class_1) + len(word_class_2) + len(word_class_3)):
        color = 'orangered'
    else:
        color = 'teal'

    x, y = reduced_vecs[i,0], reduced_vecs[i,1]
    plt.plot(x, y, marker='o', color=color, markersize=0)
    text = plt.text(x, y, anotate_all_class[i], fontdict=font, color=color, verticalalignment='center', horizontalalignment='center')
    text.set_path_effects([path_effects.Stroke(linewidth=1, foreground='tan'),
                           path_effects.Normal()])

plt.tick_params(top='off', bottom='off', left='off', right='off', labelleft='off', labelbottom='off')
plt.show()