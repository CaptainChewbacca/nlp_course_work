import pickle
import numpy as np
from gensim.models import TfidfModel
from gensim.corpora import Dictionary
from data_processor import dataset2


def pickle_it(data, path):
	with open(path, 'wb') as f:
		pickle.dump(data, f, protocol=pickle.HIGHEST_PROTOCOL)
def unpickle_it(path):
	with open(path, 'rb') as f:
		return pickle.load(f)


dataset = dataset2.read_csv_dataset_old('dataset_1', 'train')

DICTIONARY = Dictionary.load('../files/dictionary2_10k.bz2')

def dictionary_for_fit(dataset, type = "all"):
	for text in dataset:
		if type == "all" or text[2] == type:
			yield DICTIONARY.doc2bow(dataset2.text_preprocessing(text[6]))


print("start")

model_pos = unpickle_it('../files/dictionary2_10k_pos_tfidf.bz2')
model_neg = unpickle_it('../files/dictionary2_10k_neg_tfidf.bz2')
#print(DICTIONARY.get(0))
list_of_best = []

for i in range(10000):

	pos = model_pos[i] if i in model_pos else 1
	neg = model_neg[i] if i in model_neg else 1

	delta_log = np.log2(pos/neg)
	#print(DICTIONARY.get(i),":",delta_log)
	list_of_best.append([i, DICTIONARY.get(i), delta_log])


list_of_best = sorted(list_of_best, key=lambda x: x[2])

print(list_of_best[:100])
print(list_of_best[-100:])

"""
out = dict([])
for line in dictionary_for_fit(dataset, "0"):
	#delta_tfidf = model_pos[[(i,1)]][0][1] - model_neg[[(i,1)]][0][1]
	for word in line:
		if word[0] in out:
			out[word[0]] += 1
		else:
			out[word[0]] = 1
print(out)

pickle_it(out, '../files/dictionary2_10k_neg_tfidf.bz2')
"""