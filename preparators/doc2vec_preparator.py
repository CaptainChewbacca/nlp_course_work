import gensim
import os
import logging


logging.basicConfig(level=logging.INFO)

import codecs
class MySentences(object):
	def __init__(self, dirname):
		self.dirname = dirname

	def __iter__(self):
		for fname in os.listdir(self.dirname):
			for line in codecs.open(os.path.join(self.dirname, fname), "r", "utf-8"):
				yield line.split()

def create(corpus_path_from, database_path_to):
	data = MySentences(corpus_path_from)

	print("Data create")

	model = gensim.models.Word2Vec(size=50, window=7, min_count=5, sg=1, alpha=0.025, min_alpha=0.025, workers=4)
	model.build_vocab(data)


	# model.init_sims(replace=True)
	model.save(database_path_to)

	print("Model saved")


import time


def load_and_train(corpus_path_from, database_path_to, database_path_to_end):
	model = gensim.models.Word2Vec.load(database_path_to)

	data = MySentences(corpus_path_from)

	print("Model train")

	model.train(data, total_examples=model.corpus_count, epochs=7, start_alpha=0.020, end_alpha=0.0001)

	model.save(database_path_to_end)

	print("Model saved")


def save_most_freq(n = 100000):
	pass



model = "kinopoisk_w2v_50_sg"

create('E:/Dataset_CourseWork/corpus/', 'E:/Dataset_CourseWork/vectors/{0}.bin'.format(model))

load_and_train('E:/Dataset_CourseWork/corpus/', 'E:/Dataset_CourseWork/vectors/{0}.bin'.format(model),
                            'E:/Dataset_CourseWork/vectors/{0}.bin'.format(model))

#import config
#from data_processor import dictionary_word2vec
#DICTIONARY = dictionary_word2vec.load_model(config.DATA_PATH + "vectors/kinopoisk_w2v_50_1.bin").wv


#print(DICTIONARY.index2word[99999])
import numpy as np
"""from gensim.corpora import Dictionary
dct = Dictionary.load_from_text("../files/dictionary2_100k1.txt")
dct.save('../files/dictionary2_w2v_100k.bz2')
dct.save_as_text('../files/dictionary2_w2v_100k.txt')
print(dct.doc2idx(["стремителен", "!"]))"""