from gensim.corpora import Dictionary
from data_processor import dataset2


dataset = dataset2.read_csv_dataset_old('dataset_1', 'train')

def dictionary_for_fit(dataset):
	for text in dataset:
		yield dataset2.text_preprocessing(text[6])


dct = Dictionary(dictionary_for_fit(dataset))
dct.filter_extremes(keep_n = 99999)
dct.save('./files/dictionary2_100k.bz2')
dct.save_as_text('./files/dictionary2_100k.txt')
