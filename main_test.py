import logging
import config


logging.basicConfig(level=logging.INFO)


def launchTensorBoard():
	import os
	os.system('tensorboard --logdir=' + config.DATA_PATH + 'tensorflow_logs/')
	return

import threading
t = threading.Thread(target=launchTensorBoard, args=([]))
t.start()




#from ml2.models import charbychar_model, lstm_model, lstmcnn_model, convolutional_model, residual_convolutional_model, softmax_model
from ml2.models import lstm_model

model = lstm_model.Model(load_old=True)
#model.telegram_assistant = True
print("Create dataset")


from data_processor import dataset2
#dataset_train = dataset2.read_csv_dataset('dataset_1', 'train')
#dataset_valid = dataset2.read_csv_dataset('dataset_1', 'valid')
#dataset_test = dataset2.read_csv_dataset('dataset_1', 'test')

dataset_train_2 = dataset2.read_csv_dataset('dataset_2_binary_polarity', 'train')
dataset_valid_2 = dataset2.read_csv_dataset('dataset_2_binary_polarity', 'valid')
dataset_test_2 = dataset2.read_csv_dataset('dataset_2_binary_polarity', 'test')

print("Train")
#model.train(dataset_train_2, dataset_valid_2)
model.test(dataset_test_2)