import os
import shelve
import requests

dir_path = os.path.dirname(os.path.realpath(__file__))

CHAT_ID = 162090054
TOKEN = '434135562:AAFTwGiKqEKk3-1K6NPSxp8WwGQfFvldt4o'  # токен вашего бота, полученный от @BotFather



def _telegram_api_call(method, files=None, data=None):
	URL = 'https://api.telegram.org/bot'  # URL на который отправляется запрос
	try:
		request = requests.post(URL + TOKEN + '/' + method, files=files, data=data)
	except:
		return False


	if not request.status_code == 200: return False  # проверяем пришедший статус ответа
	if not request.json()['ok']: return False
	return request.json()


def send(text):
	data = {'chat_id': CHAT_ID, 'text': text, 'parse_mode': 'markdown'}
	json = _telegram_api_call('sendMessage', data=data)
	if not json:
		return False

	message_id = json['result']['message_id']
	last_chat_id = json['result']['chat']['id']

	tg_base = shelve.open(dir_path + '/telegram_notification.db')
	tg_base['last_text_message_id'] = message_id
	tg_base['last_text_chat_id'] = last_chat_id
	tg_base['last_text_message_text'] = text
	tg_base.close()
	return True


def edit(text):
	try:
		tg_base = shelve.open(dir_path + '/telegram_notification.db')
		last_message_id = tg_base['last_text_message_id']
		last_chat_id = tg_base['last_text_chat_id']
	except:
		return False
	data = {'chat_id': last_chat_id, 'message_id': last_message_id, 'text': text, 'parse_mode': 'markdown'}
	json = _telegram_api_call('editMessageText', data=data)
	if not json:
		return False
	tg_base['last_text_message_text'] = text
	tg_base.close()
	return True


def append(text):
	try:
		tg_base = shelve.open(dir_path + '/telegram_notification.db')
		last_message_id = tg_base['last_text_message_id']
		last_chat_id = tg_base['last_text_chat_id']
		last_text_message_text = tg_base['last_text_message_text']
	except:
		return False
	data = {'chat_id': last_chat_id, 'message_id': last_message_id, 'text': last_text_message_text + text, 'parse_mode': 'markdown'}
	json = _telegram_api_call('editMessageText', data=data)
	if not json:
		return False
	tg_base['last_text_message_text'] = last_text_message_text + text
	tg_base.close()
	return True

"""
def sendImage(path):
	files = {'photo': open(dir_path+'/images/' + path, 'rb')}
	data = {'chat_id': CHAT_ID}
	json = _telegram_api_call('sendPhoto', files=files, data=data)
	if not json:
		return False
	message_id = json['result']['message_id']
	last_chat_id = json['result']['chat']['id']
	tg_base = shelve.open(dir_path + '/telegram_notification.db')
	tg_base['last_photo_message_id'] = message_id
	tg_base['last_photo_chat_id'] = last_chat_id
	tg_base.close()
	return True
"""

