import numpy as np
import tensorflow as tf
import math


from gensim.corpora import Dictionary
from gensim.models import TfidfModel

import config
from data_processor import dataset2, dictionary_word2vec
from ml2.models import model

import pickle
def unpickle_it(path):
	with open(path, 'rb') as f:
		return pickle.load(f)

class Model(model.Model):
	DICTIONARY = Dictionary.load('./files/dictionary2_10k.bz2')
	MODEL_TFIDF = TfidfModel.load('./files/dictionary2_10k_tfidf.bz2')
	MODEL_POS = unpickle_it('./files/dictionary2_10k_pos_tfidf.bz2')
	MODEL_NEG = unpickle_it('./files/dictionary2_10k_neg_tfidf.bz2')

	def __init__(self, name="multilayer_perceptron_delta_tfidf_model", load_old=False):
		tf.reset_default_graph()

		learning_rate = 0.0001


		self.iterator = tf.data.Iterator.from_structure(
			{'text_vector': tf.float32, 'label': tf.float32},
			{'text_vector': tf.TensorShape([tf.Dimension(None), tf.Dimension(10000)]),
		    'label': tf.TensorShape([tf.Dimension(None), tf.Dimension(3)])}
		)
		data = self.iterator.get_next()

		self.x, self.y = data['text_vector'], data['label']
		self.phase_train = tf.placeholder(tf.bool)

		self.W, self.B = [[],[],[],[]], [[],[],[],[]]
		self.W[0], self.B[0] = self._get_WB(10000, 1000, "1")
		self.W[1], self.B[1] = self._get_WB(1000, 100, "2")
		self.W[2], self.B[2] = self._get_WB(100, 50, "3")
		self.W[3], self.B[3] = self._get_WB(50, 3, "4")

		def neural_net(x):
			layer_n = x
			M = len(self.W)
			for i in range(M - 1):
				layer_n = tf.add(tf.matmul(layer_n, self.W[i]), self.B[i])
				layer_n = tf.nn.relu(layer_n)

			out_layer = tf.matmul(layer_n, self.W[M - 1]) + self.B[M - 1]
			return out_layer


		logits = neural_net(self._batch_norm(self.x))


		with tf.variable_scope("loss"):
			self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=self.y))
			self.loss = self.loss #+ 0.001 * self._decay()

		with tf.variable_scope("optimizer"):
			self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(self.loss)

		self.true_label = tf.argmax(self.y, 1)
		self.pred = tf.argmax(logits, 1)
		self.correct_pred = tf.equal(self.pred, self.true_label)
		self.accuracy = tf.reduce_mean(tf.cast(self.correct_pred, tf.float32))
		self.logits_out = tf.nn.softmax(logits)


		model.Model.__init__(self, name, load_old, self.iterator)

		tf.summary.scalar("loss", self.loss)
		tf.summary.scalar("accuracy", self.accuracy)
		self.merged = tf.summary.merge_all()


	def _decay(self):
		costs = []
		for var in tf.trainable_variables():
			if var.op.name.find(r'W_') > 0:
				costs.append(tf.nn.l2_loss(var))
		return tf.add_n(costs)


	def _get_WB(self, input, output, layer_name):
		w = tf.get_variable("W_"+layer_name, shape=[input, output], initializer=tf.keras.initializers.he_normal())
		b = tf.get_variable("B_"+layer_name, shape=[output], initializer=tf.zeros_initializer())
		return w, b


	def _batch_norm(self, tensor, scope='bn'):
		n_out = tensor.get_shape()[-1]
		with tf.variable_scope(scope):
			beta = tf.Variable(tf.constant(0.0, shape=[n_out]),
			                   name='beta', trainable=True)
			gamma = tf.Variable(tf.constant(1.0, shape=[n_out]),
			                    name='gamma', trainable=True)
			batch_mean, batch_var = tf.nn.moments(tensor, 0, name='moments')
			ema = tf.train.ExponentialMovingAverage(decay=0.95)

			def mean_var_with_update():
				ema_apply_op = ema.apply([batch_mean, batch_var])
				with tf.control_dependencies([ema_apply_op]):
					return tf.identity(batch_mean), tf.identity(batch_var)

			mean, var = tf.cond(self.phase_train,
			                    mean_var_with_update,
			                    lambda: (ema.average(batch_mean), ema.average(batch_var)))
			normed = tf.nn.batch_normalization(tensor, mean, var, beta, gamma, 1e-3)
		return normed



	def _dataset_map_and_batching_tfidf(self, dataset, batch_size):
		def base_process_line(line):
			import tensorflow as tf
			user_id, comment_id, type, accepted, date, rate, text = tf.decode_csv(line,
			                                                                      [[0], [0], [0], [0], [''], [''],
			                                                                       ['']])

			def process(x):
				x = dataset2.text_preprocessing(x.decode("cp1251"))

				dct = self.DICTIONARY.doc2bow(x)
				out = np.zeros(shape=(10000,), dtype=np.float32)

				tfidf = self.MODEL_TFIDF[dct]

				for y in dct:
					out[y[0]] = y[1]
				# TODO сделанно для коллаба, там почему-то все int-ы 64 битные
				return [out]

			text = tf.py_func(process, [text], [tf.float32])
			label = type
			one_hot = tf.one_hot(label, 3)
			return {'text_vector': text[0], 'label': one_hot}

		return dataset.map(base_process_line). \
			padded_batch(batch_size=batch_size, padded_shapes={'text_vector': [10000], 'label': [3]})


	def _dataset_map_and_batching_delta_tfidf(self, dataset, batch_size):
		def base_process_line(line):
			import tensorflow as tf
			user_id, comment_id, type, accepted, date, rate, text = tf.decode_csv(line,
			                                                                      [[0], [0], [0], [0], [''], [''],
			                                                                       ['']])

			def process(x):
				x = dataset2.text_preprocessing(x.decode("cp1251"))

				dct = self.DICTIONARY.doc2bow(x)
				out = np.zeros(shape=(10000,), dtype=np.float32)

				len_text = 0
				for y in dct:
					len_text += y[1]

				for y in dct:
					pos = self.MODEL_POS[y[0]] if y[0] in self.MODEL_POS else 1
					neg = self.MODEL_NEG[y[0]] if y[0] in self.MODEL_NEG else 1
					delta_log = np.log2(pos / neg)
					out[y[0]] = delta_log * y[1]/len_text
				# TODO сделанно для коллаба, там почему-то все int-ы 64 битные
				return [out]

			text = tf.py_func(process, [text], [tf.float32])
			label = type
			one_hot = tf.one_hot(label, 3)
			return {'text_vector': text[0], 'label': one_hot}

		return dataset.map(base_process_line). \
			padded_batch(batch_size=batch_size, padded_shapes={'text_vector': [10000], 'label': [3]})



	def _dataset_map_and_batching(self, dataset, batch_size):
		def base_process_line(line):
			import tensorflow as tf
			user_id, comment_id, type, accepted, date, rate, text = tf.decode_csv(line,
			                                                    [[0], [0], [0], [0], [''], [''], ['']])

			def process(x):
				x = dataset2.text_preprocessing(x.decode("cp1251"))
				dct = self.DICTIONARY.doc2idx(x)
				out = np.zeros(shape=(10000,), dtype=np.float32)
				x = []
				for y in dct:
					if y == -1:
						continue
					else:
						out[y] += 1
				#TODO сделанно для коллаба, там почему-то все int-ы 64 битные
				return [out]

			text = tf.py_func(process, [text], [tf.float32])
			label = type
			one_hot = tf.one_hot(label, 3)
			return {'text_vector': text[0], 'label': one_hot}

		return dataset.map(base_process_line).\
			padded_batch(batch_size=batch_size, padded_shapes={'text_vector': [10000], 'label': [3]})


	def train(self, train_dataset, valid_dataset):
		def train_generator():
			_, summary, pred, true_label = self.main_session.run([self.optimizer, self.merged, self.pred, self.true_label],
			                                         feed_dict={self.phase_train: True})
			return summary, pred, true_label


		def valid_generator():
			accuracy, pred, true_label = self.main_session.run([self.accuracy, self.pred, self.true_label],
			                                       feed_dict={self.phase_train: False})
			return accuracy, pred, true_label


		self.__train_classifier(train_generator, train_dataset, valid_generator, valid_dataset,
		                        self._dataset_map_and_batching_delta_tfidf,
		                        train_epochs=20,
		                        train_batch_size=100,
		                        valid_size=5000,
		                        valid_batch_size=200,
		                        training_cm_iter_size=10)


	def test(self, test_data):
		def test_generator():
			accuracy, logits_out, true_label = self.main_session.run([self.accuracy, self.logits_out, self.true_label],
			                                       feed_dict={self.phase_train: False})
			return accuracy, logits_out, true_label

		self.__test_classifier(test_generator, test_data, self._dataset_map_and_batching_delta_tfidf, test_batch_size=300)



	def run(self, eval_data_x):
		out = self.main_session.run(tf.argmax(self.pred, 1), feed_dict={self.x: eval_data_x})
		return out
