import numpy as np
import tensorflow as tf
from tensorflow.contrib.tensor_forest.python import tensor_forest
from tensorflow.python.ops import resources

from gensim.corpora import Dictionary

from data_processor import dataset2
from ml2.models import model

import pickle
def unpickle_it(path):
	with open(path, 'rb') as f:
		return pickle.load(f)

class Model(model.Model):
	DICTIONARY = Dictionary.load('./files/dictionary2_10k.bz2')
	MODEL_POS = unpickle_it('./files/dictionary2_10k_pos_tfidf.bz2')
	MODEL_NEG = unpickle_it('./files/dictionary2_10k_neg_tfidf.bz2')

	def __init__(self, name="random_forest_model", load_old=False):
		tf.reset_default_graph()

		num_classes = 3  # The 10 digits
		num_features = 10000  # Each image is 28x28 pixels
		num_trees = 20
		max_nodes = 1000



		self.iterator = tf.data.Iterator.from_structure(
			{'text_vector': tf.float32, 'label': tf.float32},
			{'text_vector': tf.TensorShape([tf.Dimension(None), tf.Dimension(10000)]),
		    'label': tf.TensorShape([tf.Dimension(None), tf.Dimension(3)])}
		)
		data = self.iterator.get_next()

		self.x, self.y = data['text_vector'], data['label']


		self.hparams = tensor_forest.ForestHParams(num_classes=num_classes,
		                                      num_features=num_features,
		                                      num_trees=num_trees,
		                                      max_nodes=max_nodes).fill()

		forest_graph = tensor_forest.RandomForestGraphs(self.hparams)
		# Get training graph and loss
		self.optimizer = forest_graph.training_graph(self.x, tf.argmax(self.y, 1))
		self.loss = forest_graph.training_loss(self.x, tf.argmax(self.y, 1))

		self.true_label = tf.argmax(self.y, 1)
		infer_op, _, _ = forest_graph.inference_graph(self.x)
		self.pred = tf.argmax(infer_op, 1)
		self.correct_pred = tf.equal(self.pred, self.true_label)
		self.accuracy = tf.reduce_mean(tf.cast(self.correct_pred, tf.float32))

		model.Model.__init__(self, name, load_old, self.iterator)

		self.main_session.run(tf.group(tf.global_variables_initializer(),
		                               resources.initialize_resources(resources.shared_resources())))
		tf.summary.scalar("loss", self.loss)
		tf.summary.scalar("accuracy", self.accuracy)
		self.merged = tf.summary.merge_all()



	def _dataset_map_and_batching_delta_tfidf(self, dataset, batch_size):
		def base_process_line(line):
			import tensorflow as tf
			user_id, comment_id, type, accepted, date, rate, text = tf.decode_csv(line,
			                                                                      [[0], [0], [0], [0], [''], [''],
			                                                                       ['']])

			def process(x):
				x = dataset2.text_preprocessing(x.decode("cp1251"))

				dct = self.DICTIONARY.doc2bow(x)
				out = np.zeros(shape=(10000,), dtype=np.float32)

				len_text = 0
				for y in dct:
					len_text += y[1]

				for y in dct:
					pos = self.MODEL_POS[y[0]] if y[0] in self.MODEL_POS else 1
					neg = self.MODEL_NEG[y[0]] if y[0] in self.MODEL_NEG else 1
					delta_log = np.log2(pos / neg)
					out[y[0]] = delta_log * y[1]/len_text
				# TODO сделанно для коллаба, там почему-то все int-ы 64 битные
				return [out]

			text = tf.py_func(process, [text], [tf.float32])
			label = type
			one_hot = tf.one_hot(label, 3)
			return {'text_vector': text[0], 'label': one_hot}

		return dataset.map(base_process_line). \
			padded_batch(batch_size=batch_size, padded_shapes={'text_vector': [10000], 'label': [3]})



	def _dataset_map_and_batching(self, dataset, batch_size):
		def base_process_line(line):
			import tensorflow as tf
			user_id, comment_id, type, accepted, date, rate, text = tf.decode_csv(line,
			                                                    [[0], [0], [0], [0], [''], [''], ['']])

			def process(x):
				x = dataset2.text_preprocessing(x.decode("cp1251"))
				dct = self.DICTIONARY.doc2idx(x)
				out = np.zeros(shape=(10000,), dtype=np.float32)
				x = []
				for y in dct:
					if y == -1:
						continue
					else:
						out[y] = 1
				#TODO сделанно для коллаба, там почему-то все int-ы 64 битные
				return [out]

			text = tf.py_func(process, [text], [tf.float32])
			label = type
			one_hot = tf.one_hot(label, 3)
			return {'text_vector': text[0], 'label': one_hot}

		return dataset.map(base_process_line).\
			padded_batch(batch_size=batch_size, padded_shapes={'text_vector': [10000], 'label': [3]})


	def train(self, train_dataset, valid_dataset):

		def train_generator():
			_, _, summary, true_label, pred = self.main_session.run([self.optimizer, self.loss, self.merged, self.true_label, self.pred],
			                                         feed_dict={})
			return summary, pred, true_label


		def valid_generator():
			accuracy, true_label, pred = self.main_session.run([self.accuracy, self.true_label, self.pred],
			                                       feed_dict={})
			return accuracy, pred, true_label


		self.__train_classifier(train_generator, train_dataset, valid_generator, valid_dataset,
		                        self._dataset_map_and_batching,
		                        train_epochs=5,
		                        train_batch_size=500,
		                        valid_size=5000,
		                        valid_batch_size=500,
		                        training_cm_iter_size=10)




	def test(self, test_data):
		def test_generator():
			accuracy, true_label, pred = self.main_session.run([self.accuracy, self.true_label, self.pred],
			                                       feed_dict={})
			return accuracy, pred, true_label

		self.__test_classifier(test_generator, test_data, self._dataset_map_and_batching, test_batch_size=100)



	def run(self, eval_data_x):
		out = self.main_session.run(self.pred, feed_dict={self.x: eval_data_x})
		return out

