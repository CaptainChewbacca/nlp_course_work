
import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn

from gensim.corpora import Dictionary

import config
from data_processor import dataset2, dictionary_word2vec
from ml2.models import model



DICTIONARY_id = Dictionary.load('./files/dictionary2_w2v_100k.bz2')
DICTIONARY = dictionary_word2vec.load_model(config.DATA_PATH + "vectors/kinopoisk_w2v_50_cbow.bin").wv

class Model(model.Model):
	def __init__(self, name="resnet_model", load_old=False):
		tf.reset_default_graph()

		learning_rate = 0.001


		self.iterator = tf.data.Iterator.from_structure(
			{'text': tf.int32, 'text_size': tf.int32, 'label': tf.float32},
			{'text': tf.TensorShape([tf.Dimension(None), tf.Dimension(None)]), 'text_size': tf.TensorShape([tf.Dimension(None)]),
		    'label': tf.TensorShape([tf.Dimension(None), tf.Dimension(3)])}
		)
		data = self.iterator.get_next()

		self.dropout_rate = 0.1
		self.x, self.y, self.length_x = data['text'], data['label'], data['text_size']

		self.phase_train = tf.placeholder(tf.bool)
		self.keep_prob = tf.placeholder_with_default(tf.constant(1.0, dtype=tf.float32), ())


		with tf.device('/cpu:0'), tf.name_scope("embedding"):
			self.saved_embeddings = tf.constant(DICTIONARY.syn0[0:100000])
			self.embedding_encoder = tf.Variable(initial_value=self.saved_embeddings,
			            trainable=True, name="embedding_encoder")
			self.emb_input = tf.nn.embedding_lookup(self.embedding_encoder, self.x)




		with tf.variable_scope("resnet"):
			conv_network = self._batch_norm(self.emb_input)

			conv_network = self._create_conv1d(conv_network, 7, 50, 64, 3, "1")
			conv_network = self._create_polling(conv_network, 2, 2)

			conv_network = self.resnet_block(conv_network, 3, 64, "2")
			conv_network = self.resnet_block(conv_network, 3, 64, "3")
			conv_network = self.resnet_block(conv_network, 3, 64, "4")

			conv_network = self.resnet_block(conv_network, 3, 64, "5", reduce_layer=True)

			conv_network = self.resnet_block(conv_network, 3, 128, "6")
			conv_network = self.resnet_block(conv_network, 3, 128, "7")
			conv_network = self.resnet_block(conv_network, 3, 128, "8")

			conv_network = self.resnet_block(conv_network, 3, 128, "9", reduce_layer=True)

			conv_network = self.resnet_block(conv_network, 3, 256, "10")
			conv_network = self.resnet_block(conv_network, 3, 256, "11")
			conv_network = self.resnet_block(conv_network, 3, 256, "12")

			conv_network = tf.reduce_mean(conv_network, 1)

			w10, b10 = self._get_WB(256, 1024, "out_1")
			w11, b11 = self._get_WB(1024, 3, "out_2")

			conv_network = tf.add(tf.matmul(conv_network, w10), b10)
			conv_network = tf.nn.relu(conv_network)

			#Целевая фунция
			logits = tf.add(tf.matmul(conv_network, w11), b11)



		with tf.variable_scope("loss"):
			self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=self.y))
			self.loss = self.loss + 0.001 * self._decay()

		with tf.variable_scope("optimizer"):
			self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(self.loss)

		self.true_label = tf.argmax(self.y, 1)
		self.pred = tf.argmax(logits, 1)
		self.correct_pred = tf.equal(self.pred, self.true_label)
		self.accuracy = tf.reduce_mean(tf.cast(self.correct_pred, tf.float32))

		model.Model.__init__(self, name, load_old, self.iterator)

		tf.summary.scalar("loss", self.loss)
		tf.summary.scalar("accuracy", self.accuracy)
		self.merged = tf.summary.merge_all()


	def _decay(self):
		costs = []
		for var in tf.trainable_variables():
			if var.op.name.find(r'W_') > 0:
				costs.append(tf.nn.l2_loss(var))
		return tf.add_n(costs)

	def _get_WB(self, input, output, layer_name):
		w = tf.get_variable("W_"+layer_name, shape=[input, output], initializer=tf.keras.initializers.he_normal())
		b = tf.get_variable("B_"+layer_name, shape=[output], initializer=tf.zeros_initializer())
		return w, b

	def _get_filter_WB(self, width, input, output, layer_name):
		w = tf.get_variable("W_"+layer_name, shape=[width, input, output],
		                    initializer=tf.truncated_normal_initializer(0.0, np.sqrt(2.0/(width*input))))
		b = tf.get_variable("B_"+layer_name, shape=[output], initializer=tf.zeros_initializer())
		return w, b

	def _batch_norm(self, tensor, axis=[0, 1], scope='bn'):
		n_out = tensor.get_shape()[-1]
		with tf.variable_scope(scope):
			beta = tf.Variable(tf.constant(0.0, shape=[n_out]),
			                   name='beta', trainable=True)
			gamma = tf.Variable(tf.constant(1.0, shape=[n_out]),
			                    name='gamma', trainable=True)
			batch_mean, batch_var = tf.nn.moments(tensor, axis, name='moments')
			ema = tf.train.ExponentialMovingAverage(decay=0.95)

			def mean_var_with_update():
				ema_apply_op = ema.apply([batch_mean, batch_var])
				with tf.control_dependencies([ema_apply_op]):
					return tf.identity(batch_mean), tf.identity(batch_var)

			mean, var = tf.cond(self.phase_train,
			                    mean_var_with_update,
			                    lambda: (ema.average(batch_mean), ema.average(batch_var)))
			normed = tf.nn.batch_normalization(tensor, mean, var, beta, gamma, 1e-3)
		return normed


	def _create_conv1d(self, tensor, width, input, output, stride, layer_name, dropout=0.0):
		w, b = self._get_filter_WB(width, input, output, layer_name)
		tensor = tf.nn.conv1d(tensor, w, stride=stride, padding='SAME')
		tensor = tf.nn.bias_add(tensor, b)
		tensor = self._batch_norm(tensor)
		tensor = tf.nn.relu(tensor)
		return tensor


	def resnet_block(self, tensor, width, input, layer_name, reduce_layer=False, dropout=0.0):

		output = input if not reduce_layer else input*2

		w1, b1 = self._get_filter_WB(width, input, output, layer_name+"_conv1")
		w2, b2 = self._get_filter_WB(width, output, output, layer_name+"_conv2")

		old_tensor = tensor

		tensor = self._batch_norm(tensor)
		tensor = tf.nn.relu(tensor)


		stride_1 = 2 if reduce_layer else 1
		tensor = tf.nn.conv1d(tensor, w1, stride=stride_1, padding='SAME')
		tensor = tf.nn.bias_add(tensor, b1)

		tensor = self._batch_norm(tensor)
		tensor = tf.nn.relu(tensor)

		tensor = tf.nn.conv1d(tensor, w2, stride=1, padding='SAME')
		tensor = tf.nn.bias_add(tensor, b2)

		if reduce_layer:
			w3, b3 = self._get_filter_WB(width, input, output, layer_name + "_conv_skipconnection")
			old_tensor = tf.nn.conv1d(old_tensor, w3, stride=2, padding='SAME')
			old_tensor = tf.nn.bias_add(old_tensor, b3)

		return  tf.add(tensor, old_tensor)


	def _create_polling(self, tensor, window_size=2, stride=2, type="MAX"):
		#return tf.nn.max_pool(conv_network, ksize=[1, 1, size, 1], strides=[1, 1, size, 1], padding='SAME')
		return tf.nn.pool(tensor, window_shape=[window_size], strides=[stride], padding='SAME', pooling_type=type)



	def train(self, train_dataset, valid_dataset):

		def train_generator():
			_, summary, true_label, pred = self.main_session.run([self.optimizer, self.merged, self.true_label, self.pred],
			                                         feed_dict={
				                                                self.phase_train: True,
			                                                    self.keep_prob: 0.8
			                                         }
			                                         )
			return summary, pred, true_label


		def valid_generator():
			accuracy, true_label, pred = self.main_session.run([self.accuracy, self.true_label, self.pred],
			                                       feed_dict={self.phase_train: False})
			return accuracy, pred, true_label


		def dataset_map_and_batching(dataset, batch_size):
			def base_process_line(line):
				import tensorflow as tf
				user_id, comment_id, type, accepted, date, rate, text = tf.decode_csv(line,
				                                                    [[0], [0], [0], [0], [''], [''], ['']])

				def process(x):
					x = dataset2.text_preprocessing(x.decode("cp1251"))
					dct = DICTIONARY_id.doc2idx(x)
					x = []
					for y in dct:
						if y == -1:
							continue
						else:
							x.append(y)
						if len(x) >= 1000:
							break
					#TODO сделанно для коллаба, там почему-то все int-ы 64 битные
					return [np.array(x, dtype=np.int32), np.array([len(x)], dtype=np.int32)[0]]
				text = tf.py_func(process, [text], [tf.int32, tf.int32])
				label = type
				one_hot = tf.one_hot(label, 3)
				return {'text': text[0], 'text_size': text[1], 'label': one_hot}

			return dataset.map(base_process_line).\
				padded_batch(batch_size=batch_size, padded_shapes={'text': [None], 'text_size': [], 'label': [3]})


		self.__train_classifier(train_generator, train_dataset, valid_generator, valid_dataset,
		                        dataset_map_and_batching,
		                        train_epochs=1000,
		                        train_batch_size=100,
		                        valid_size=5000,
		                        valid_batch_size=100,
		                        training_cm_iter_size=10)

	def test(self, test_data):
		def test_generator(test_data, i, batch_size):
			batch_xs = test_data[0][i * batch_size: i * batch_size + batch_size]
			batch_length_xs = [len(x) for x in batch_xs]

			batch_xs = [np.pad(x, ((0, 500 - len(x)), (0, 0)), 'constant') for x in batch_xs]
			pred = self.main_session.run(self.pred, feed_dict={self.x: batch_xs, self.length_x: batch_length_xs,
			                                                        self.phase_train: False})
			return pred
		self.__test_classifier(test_generator, test_data, batch_size=100)


	def run(self, data_x):
		out = self.main_session.run(self.pred, feed_dict={self.x: data_x, self.phase_train: False})
		return out



