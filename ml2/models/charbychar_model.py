import numpy as np
import tensorflow as tf

from tensorflow.contrib import rnn

from data_processor import dataset2
from ml2.models import model





class Model(model.Model):
	def __init__(self, name="charbychar_model", load_old=False):
		tf.reset_default_graph()

		learning_rate = 0.001
		dropout_rate = 0.5

		self.iterator = tf.data.Iterator.from_structure(
			{'text': tf.int32, 'text_size': tf.int32, 'label': tf.float32},
			{'text': tf.TensorShape([tf.Dimension(None), tf.Dimension(None)]), 'text_size': tf.TensorShape([tf.Dimension(None)]),
		    'label': tf.TensorShape([tf.Dimension(None), tf.Dimension(3)])}
		)

		data = self.iterator.get_next()

		self.x, self.y, self.length_x = data['text'], data['label'], data['text_size']

		self.phase_train = tf.placeholder(tf.bool)
		self.keep_prob = tf.placeholder_with_default(tf.constant(1.0, dtype=tf.float32), ())


		self.embedding_encoder = tf.get_variable("embedding_encoder", shape=[80, 10],
		                                         initializer=tf.random_uniform_initializer(-1, 1), trainable=True)
		self.emb_input = tf.nn.embedding_lookup(self.embedding_encoder, self.x)



		with tf.variable_scope("rnn"):
			w1, b1 = self._get_WB(80, 3, 'out_1')

			outputs, states = tf.nn.dynamic_rnn(self._create_layer(80, self.keep_prob),
			                                    self.emb_input, dtype=tf.float32, sequence_length=self.length_x)

			network = tf.add(tf.matmul(states, w1), b1)
			logits = network


		with tf.variable_scope("loss"):
			self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=self.y))
			self.loss = self.loss + 0.001 * self._decay()

		with tf.variable_scope("optimizer"):
			self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(self.loss)


		self.true_label = tf.argmax(self.y, 1)
		self.pred = tf.argmax(logits, 1)
		self.correct_pred = tf.equal(self.pred, self.true_label)
		self.accuracy = tf.reduce_mean(tf.cast(self.correct_pred, tf.float32))

		model.Model.__init__(self, name, load_old, iterator=self.iterator)

		tf.summary.scalar("loss", self.loss)
		tf.summary.scalar("accuracy", self.accuracy)
		self.merged = tf.summary.merge_all()


	def _decay(self):
		costs = []
		for var in tf.trainable_variables():
			if var.op.name.find(r'W_') > 0:
				costs.append(tf.nn.l2_loss(var))
		return tf.add_n(costs)

	def _get_WB(self, input, output, layer_name):
		w = tf.get_variable("W_" + layer_name, shape=[input, output], initializer=tf.keras.initializers.he_normal())
		b = tf.get_variable("B_" + layer_name, shape=[output], initializer=tf.zeros_initializer())
		return w, b

	def _create_layer(self, size, dropout):
		cell = rnn.GRUCell(size)
		cell = rnn.DropoutWrapper(cell, input_keep_prob=dropout, state_keep_prob=dropout,
		                                          output_keep_prob=dropout)
		return cell



	def train(self, train_dataset, valid_dataset):

		def train_generator():
			_, summary, true_label, pred = self.main_session.run([self.optimizer, self.merged, self.true_label, self.pred],
			                                         feed_dict={
				                                                self.phase_train: True,
			                                                    self.keep_prob: 0.8
			                                         }
			                                         )
			return summary, pred, true_label


		def valid_generator():
			accuracy, true_label, pred = self.main_session.run([self.accuracy, self.true_label, self.pred],
			                                       feed_dict={self.phase_train: False})
			return accuracy, pred, true_label


		def dataset_map_and_batching(dataset, batch_size):
			def base_process_line(line):
				import tensorflow as tf
				user_id, comment_id, type, accepted, date, rate, text = tf.decode_csv(line,
				                                                    [[0], [0], [0], [0], [''], [''], ['']])
				def process(x):
					x = dataset2.text_preprocessing(x.decode("cp1251"))
					x, len = dataset2.generate_char_vector_data(x)
					return [x, len]
				text = tf.py_func(process, [text], [tf.int32, tf.int32])
				label = type
				one_hot = tf.one_hot(label, 3)
				return {'text': text[0], 'text_size': text[1], 'label': one_hot}

			return dataset.map(base_process_line).\
				padded_batch(batch_size=batch_size, padded_shapes={'text': [None], 'text_size': [], 'label': [3]})


		self.__train_classifier(train_generator, train_dataset, valid_generator, valid_dataset,
		                        dataset_map_and_batching,
		                        train_epochs=20,
		                        train_batch_size=20,
		                        valid_size=1000,
		                        valid_batch_size=50,
		                        training_cm_iter_size=10)


	def test(self, test_data):
		def test_generator(test_data, i, batch_size):
			batch_xs = test_data[0][i * batch_size: i * batch_size + batch_size]
			batch_length_xs = [len(x) for x in batch_xs]

			batch_xs = [np.pad(x, (0, 2000 - len(x)), 'constant') for x in batch_xs]
			pred = self.main_session.run(self.pred, feed_dict={self.x: batch_xs, self.length_x: batch_length_xs,
			                                                        self.phase_train: False})
			return pred
		self.__test_classifier(test_generator, test_data, batch_size=10)


	def run(self, data_x):
		out = self.main_session.run(self.pred, feed_dict={self.x: data_x, self.phase_train: False})
		return out
