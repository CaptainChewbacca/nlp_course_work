import logging
import numpy as np
import tensorflow as tf
import config
from data_drawer import tensorboard_wrapper
from utils.telegram_assistant import telegram_notifications
import pickle

def pickle_it(data, path):
	with open(path, 'wb') as f:
		pickle.dump(data, f, protocol=pickle.HIGHEST_PROTOCOL)
def unpickle_it(path):
	with open(path, 'rb') as f:
		return pickle.load(f)

class Model:

	def __init__(self, name, load_old, iterator):
		self.name = name
		self.path = config.DATA_PATH + "models/" + name + "/model"
		self.saver = tf.train.Saver()
		self.main_session = tf.Session()
		init = tf.global_variables_initializer()
		self.main_session.run(init)
		self.iterator = iterator

		self.telegram_assistant = False

		if load_old:
			self.load()

	def load(self):
		if self.path:
			try:
				self.saver.restore(self.main_session, self.path)
			except Exception as ex:
				print("Ошибка при загрузке модели (Не существует)")

	def save(self):
		if self.path:
			self.saver.save(self.main_session, self.path)

	def __train_classifier(self, train_generator, train_dataset,
	                       valid_generator, valid_dataset,
	                       dataset_map_and_batching,
	                       train_epochs=20,
	                       train_batch_size=500,
	                       valid_size=1000,
	                       valid_batch_size=500,
	                       epoch_iterations=100,
	                       training_cm_iter_size=10):

		train_writer = tf.summary.FileWriter(config.TF_LOG_DIR.format(self.name) + '/train',
		                                     graph=tf.get_default_graph())
		valid_writer = tf.summary.FileWriter(config.TF_LOG_DIR.format(self.name) + '/valid',
		                                     graph=tf.get_default_graph())
		logging.info("Start train with: {0} training epochs, {1} batch_size".format(train_epochs, train_batch_size))
		if self.telegram_assistant:
			telegram_notifications.send(
				"Model *{0}* start train ({1} epochs, {2} iterations by epoch, {3} train batch size)\n".format(
					self.name, train_epochs, epoch_iterations, train_batch_size)
			)

		last_valid_accuracy = 0


		train_dataset = dataset_map_and_batching(train_dataset, train_batch_size)
		valid_dataset = dataset_map_and_batching(valid_dataset, valid_batch_size)


		train_dataset = train_dataset.repeat(-1)
		valid_dataset = valid_dataset.repeat(1)


		valid_init_op = self.iterator.make_initializer(valid_dataset)

		for epoch in range(train_epochs):
			train_init_op = self.iterator.make_initializer(train_dataset.skip(epoch * epoch_iterations))
			self.main_session.run(train_init_op)

			confusion_matrix = np.zeros(shape=(3, 3), dtype=int)

			for step in range(epoch_iterations):
				k = epoch * epoch_iterations + step
				summary, pred, true_label = train_generator()
				train_writer.add_summary(summary, k)

				for i in range(len(true_label)):
					confusion_matrix[true_label[i]][pred[i]] += 1

				if (k % training_cm_iter_size) == 0:
					img_d_summary = tensorboard_wrapper.plot_confusion_matrix(confusion_matrix,
					                                                          ["Отриц.", "Нейтр.", "Полож."],
					                                                          tensor_name='dev/cm')
					train_writer.add_summary(img_d_summary, k)
					confusion_matrix.fill(0)

					if self.telegram_assistant:
						telegram_notifications.append("\n- Epoch: *{0}*, step: {1}".format(epoch + 1, step + 1))

				logging.info("Epoch: {0}, step: {1}".format(epoch + 1, step + 1))

			# Test phase
			self.main_session.run(valid_init_op)

			accuracies, confusion_matrix = [], np.zeros(shape=(3, 3), dtype=int)
			while True:
				try:
					accuracy, pred, true_label = valid_generator()
					accuracies.append(accuracy)
					for i in range(len(true_label)):
						confusion_matrix[true_label[i]][pred[i]] += 1
				except tf.errors.OutOfRangeError:
					break

			img_d_summary = tensorboard_wrapper.plot_confusion_matrix(confusion_matrix,
			                                                          ["Отриц.", "Нейтр.", "Полож."],
			                                                          tensor_name='dev/cm')
			valid_writer.add_summary(img_d_summary, epoch)
			accuracy = np.mean(accuracies)

			summary = tf.Summary()
			summary.value.add(tag="accuracy_validation", simple_value=accuracy)
			valid_writer.add_summary(summary, epoch)

			logging.info("Validation accuracy: {0}".format(accuracy))

			telegram_text = "\n\n➖ Validation accuracy: {0}\n".format(accuracy)

			if accuracy >= last_valid_accuracy:
				self.save()
				logging.info("Model saved, accuracy increased by: {0}".format(accuracy - last_valid_accuracy))
				telegram_text += "➖💾 Model saved, accuracy increased by: {0}\n".format(accuracy - last_valid_accuracy)

				last_valid_accuracy = accuracy

			if self.telegram_assistant:
				telegram_notifications.send(telegram_text)

		logging.info("Train finish!")

	def __test_classifier(self, test_generator, test_dataset, dataset_map_and_batching, test_batch_size=500):
		test_writer = tf.summary.FileWriter(config.TF_LOG_DIR.format(self.name) + '/test', graph=tf.get_default_graph())

		logging.info("Start test with: {0} batch_size".format(test_batch_size))

		test_dataset = dataset_map_and_batching(test_dataset, test_batch_size)
		test_dataset = test_dataset.repeat(1)
		test_init_op = self.iterator.make_initializer(test_dataset)
		self.main_session.run(test_init_op)

		accuracies, confusion_matrix = [], np.zeros(shape=(3, 3), dtype=int)

		logits_out = np.empty(shape=(0,3))

		while True:
			try:
				accuracy, logit, true_label = test_generator()
				accuracies.append(accuracy)
				for i in range(len(true_label)):
					confusion_matrix[true_label[i]][np.argmax(logit[i])] += 1
				logits_out = np.vstack((logits_out, logit))
			except tf.errors.OutOfRangeError:
				break


		pickle_it(logits_out, "./ansamble_data/"+self.name)

		img_d_summary = tensorboard_wrapper.plot_confusion_matrix(confusion_matrix,
		                                                          ["Отриц.", "Нейтр.", "Полож."],
		                                                          tensor_name='dev/cm')
		test_writer.add_summary(img_d_summary, 1)

		logging.info("Test finish!")