import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn

from ml.models import model


class Model(model.Model):
	def __init__(self, name="ensemble_lstm_model", load_old=False):
		tf.reset_default_graph()

		learning_rate = 0.001

		self.x = tf.placeholder(tf.float32, [None, 500, 200])
		self.y = tf.placeholder(tf.float32, [None, 3])
		self.length_x = tf.placeholder(tf.int32, [None])
		self.phase_train = tf.placeholder(tf.bool)
		self.keep_prob = tf.placeholder_with_default(tf.constant(1.0, dtype=tf.float32), ())


		with tf.variable_scope("rnn1"):
			w1, b1 = self._get_WB(70, 1, 'out_1')

			cells = [self._create_layer(130, self.keep_prob),
			         self._create_layer(70, self.keep_prob)]
			cells = rnn.MultiRNNCell(cells=cells)

			outputs, states = tf.nn.dynamic_rnn(cells, self.x, dtype=tf.float32, sequence_length=self.length_x)
			states = states[1].h


			network = tf.add(tf.matmul(states, w1), b1)
			logits1 = network
			print(logits1)


		with tf.variable_scope("rnn2"):
			w1, b1 = self._get_WB(70, 1, 'out_1')

			cells = [self._create_layer(130, self.keep_prob),
			         self._create_layer(70, self.keep_prob)]
			cells = rnn.MultiRNNCell(cells=cells)

			outputs, states = tf.nn.dynamic_rnn(cells, self.x, dtype=tf.float32, sequence_length=self.length_x)
			states = states[1].h


			network = tf.add(tf.matmul(states, w1), b1)
			logits2 = network

		with tf.variable_scope("rnn3"):
			w1, b1 = self._get_WB(70, 1, 'out_1')

			cells = [self._create_layer(130, self.keep_prob),
			         self._create_layer(70, self.keep_prob)]
			cells = rnn.MultiRNNCell(cells=cells)

			outputs, states = tf.nn.dynamic_rnn(cells, self.x, dtype=tf.float32, sequence_length=self.length_x)
			states = states[1].h


			network = tf.add(tf.matmul(states, w1), b1)
			logits3 = network


		with tf.variable_scope("logit"):
			logits = tf.concat([logits1, logits2], 1)
			logits = tf.concat([logits, logits3], 1)

		with tf.variable_scope("loss"):
			self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=self.y))
			self.loss = self.loss + 0.001 * self._decay()

		with tf.variable_scope("optimizer"):
			self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(self.loss)


		self.pred = tf.argmax(logits, 1)
		self.correct_pred = tf.equal(self.pred, tf.argmax(self.y, 1))
		self.accuracy = tf.reduce_mean(tf.cast(self.correct_pred, tf.float32))

		model.Model.__init__(self, name, load_old)

		tf.summary.scalar("loss", self.loss)
		tf.summary.scalar("accuracy", self.accuracy)
		self.merged = tf.summary.merge_all()


	def _decay(self):
		costs = []
		for var in tf.trainable_variables():
			if var.op.name.find(r'W_') > 0:
				costs.append(tf.nn.l2_loss(var))
		return tf.add_n(costs)

	def _get_WB(self, input, output, layer_name):
		w = tf.get_variable("W_" + layer_name, shape=[input, output], initializer=tf.keras.initializers.he_normal())
		b = tf.get_variable("B_" + layer_name, shape=[output], initializer=tf.zeros_initializer())
		return w, b

	def _create_layer(self, size, dropout):
		cell = rnn.LayerNormBasicLSTMCell(size, dropout_keep_prob=dropout)
		#cell = rnn.DropoutWrapper(cell, input_keep_prob=self.input_keep_prob, state_keep_prob=self.state_keep_prob,
		#                                          output_keep_prob=self.output_keep_prob)
		return cell


	def train(self, train_data, valid_data):

		def train_generator(train_data, i, batch_size):

			random_batch = np.random.randint(len(train_data[1]), size=batch_size)

			batch_xs = [train_data[0][i] for i in random_batch]
			batch_length_xs = [len(x) for x in batch_xs]
			batch_xs = [np.pad(x, ((0, 500 - len(x)), (0, 0)), 'constant') for x in batch_xs]

			batch_ys = [train_data[1][i] for i in random_batch]

			_, summary, pred = self.main_session.run([self.optimizer, self.merged, self.pred],
			                                         feed_dict={self.x: batch_xs, self.y: batch_ys,
			                                                    self.length_x: batch_length_xs,
			                                                    self.phase_train: True,
			                                                    self.keep_prob: 0.8})
			return summary, pred, batch_ys


		def valid_generator(valid_data, i, batch_size):
			batch_xs = valid_data[0][i * batch_size: i * batch_size + batch_size]
			batch_length_xs = [len(x) for x in batch_xs]
			batch_xs = [np.pad(x, ((0, 500 - len(x)), (0, 0)), 'constant') for x in batch_xs]

			batch_ys = valid_data[1][i * batch_size: i * batch_size + batch_size]

			accuracy, pred = self.main_session.run([self.accuracy, self.pred],
			                                       feed_dict={self.x: batch_xs, self.y: batch_ys,
			                                                  self.length_x: batch_length_xs,
			                                                  self.phase_train: False})
			return accuracy, pred, batch_ys


		self.__train_classifier(train_generator, train_data, valid_generator, valid_data,
		                        training_epochs=20, batch_size=50, training_cm_iter_size=10, epoch_iterations=100)


	def test(self, test_data):
		def test_generator(test_data, i, batch_size):
			batch_xs = test_data[0][i * batch_size: i * batch_size + batch_size]
			batch_length_xs = [len(x) for x in batch_xs]

			batch_xs = [np.pad(x, ((0, 500 - len(x)), (0, 0)), 'constant') for x in batch_xs]
			pred = self.main_session.run(self.pred, feed_dict={self.x: batch_xs, self.length_x: batch_length_xs,
			                                                        self.phase_train: False})
			return pred
		self.__test_classifier(test_generator, test_data, batch_size=100)


	def run(self, data_x):
		out = self.main_session.run(self.pred, feed_dict={self.x: data_x, self.phase_train: False})
		return out
