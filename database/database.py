import sqlite3

def create_select_query(database, sql_text):
	with sqlite3.connect(database, timeout=10) as con:
		cursor = con.cursor()
		cursor.execute(sql_text)
		out = cursor.fetchall()
	return out

def create_query(database, sql_text):
	with sqlite3.connect(database, timeout=10) as con:
		cursor = con.cursor()
		cursor.executescript(sql_text)
		con.commit()

