import numpy as np
import pickle
def pickle_it(data, path):
	with open(path, 'wb') as f:
		pickle.dump(data, f, protocol=pickle.HIGHEST_PROTOCOL)
def unpickle_it(path):
	with open(path, 'rb') as f:
		return pickle.load(f)

data = unpickle_it("data")
m1 = unpickle_it("bidirectional_lstm_model_cbow")
m2 = unpickle_it("convolution_model")
m3 = unpickle_it("lstm_model")
m4 = unpickle_it("multilayer_perceptron_delta_tfidf_model")
m5 = unpickle_it("lstmcnn_model_cbow")

end_predict = m1 + m2 + m3 + m4 + m5

confusion_matrix = np.zeros(shape=(3, 3), dtype=int)

for i in range(len(data)):
	confusion_matrix[data[i]][np.argmax(end_predict[i])] += 1

print(confusion_matrix)





import tensorflow as tf
import config
from data_drawer import tensorboard_wrapper

test_writer = tf.summary.FileWriter(config.TF_LOG_DIR.format("ANSAMBLE") + '/test', graph=tf.get_default_graph())
img_d_summary = tensorboard_wrapper.plot_confusion_matrix(confusion_matrix,
                                                          ["Отриц.", "Нейтр.", "Полож."],
                                                          tensor_name='dev/cm')
test_writer.add_summary(img_d_summary, 1)



def launchTensorBoard():
	import os
	os.system('tensorboard --logdir=' + config.DATA_PATH + 'tensorflow_logs/')
	return

import threading
t = threading.Thread(target=launchTensorBoard, args=([]))
t.start()

