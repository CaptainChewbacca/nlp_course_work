import random
import numpy as np

import config
from database import database





def generate_vector_data(data_x, words, n_gram=1):
	out_data_x = []
	for x in data_x:
		out_x = np.zeros(len(words))
		for i in range(len(x) - n_gram + 1):
			gram = ''
			for j in range(n_gram):
				gram += x[i + j] + ' '
			gram = gram[:-1]
			if gram in words:
				out_x[words[gram][3]] += words[gram][2]
		out_data_x.append(out_x)
	return out_data_x


def generate_word_vector_data(data_x, words, size=200):
	out_data = []
	for x in data_x:
		out_data_x = []
		count = 0
		for word in x:
			if count < 500:
				try:
					out_data_x.append(words[word])
					count += 1
				except:
					pass
		#print(len(out_data_x))
		if count == 0:
			out_data_x.append(size)
		out_data.append(out_data_x)
	return out_data


def generate_char_vector_data(data_x):
	out_data = []
	char_dict = {
		' ': 0, '.': 1, ',': 2, '?': 3, '!': 4, ':': 5, ';': 6, '-': 7,
		'а': 10, 'б': 11, 'в': 12, 'г': 13, 'д': 14, 'е': 15, 'ё': 16, 'ж': 17, 'з': 18, 'и': 19, 'й': 20,
		'к': 21, 'л': 22, 'м': 23, 'н': 24, 'о': 25, 'п': 26, 'р': 27, 'с': 28, 'т': 29, 'у': 30, 'ф': 31,
		'х': 32, 'ц': 33, 'ч': 34, 'ш': 35, 'щ': 36, 'ъ': 37, 'ы': 38, 'ь': 39, 'э': 40, 'ю': 41, 'я': 42,

		'a': 43, 'b': 44, 'c': 45, 'd': 46, 'e': 47, 'f': 48, 'g': 49, 'h': 50, 'i': 51, 'j': 52, 'k': 53,
		'l': 54, 'm': 55, 'n': 56, 'o': 57, 'p': 58, 'q': 59, 'r': 60, 's': 61, 't': 62, 'u': 63, 'v': 64,
		'w': 65, 'x': 66, 'y': 67, 'z': 68,

		'0': 70, '1': 71, '2': 72, '3': 73, '4': 74, '5': 75, '6': 76, '7': 77, '8': 78, '9': 79,
	}
	for x in data_x:
		source_text = ' '.join(x)
		out_data_x = []
		count = 0
		for char in source_text:
			if char in char_dict and count < 2000:
				out_data_x.append(char_dict[char])
				count += 1
		out_data.append(out_data_x)
	return out_data



def text_preprocessing(text):
	delete_symbol = """(){}[]|&"""
	punctuation = """.,?!:;-"""
	text = text.lower().replace('\n', ' ')
	text = text.replace('|', '.')
	text = text.replace('<br />', ' ')

	for c in delete_symbol:
		text = text.replace(c, ' ')

	for c in punctuation:
		text = text.replace(c, ' ' + c + ' ')

	return text.split()




class DataSet:

	def __init__(self, dataset):
		self.TrainData = dataset[0]
		self.ValidationData = dataset[1]
		self.TestData = dataset[2]

	def prepare_vectors(self, words, n_grams=1):
		self.TrainData[0] = generate_vector_data(self.TrainData[0], words, n_grams)
		self.ValidationData[0] = generate_vector_data(self.ValidationData[0], words, n_grams)
		self.TestData[0] = generate_vector_data(self.TestData[0], words, n_grams)

	def prepare_word_vectors(self, words, size=200):
		self.TrainData[0] = generate_word_vector_data(self.TrainData[0], words, size)
		self.ValidationData[0] = generate_word_vector_data(self.ValidationData[0], words, size)
		self.TestData[0] = generate_word_vector_data(self.TestData[0], words, size)

	def prepare_char_vectors(self):
		self.TrainData[0] = generate_char_vector_data(self.TrainData[0])
		self.ValidationData[0] = generate_char_vector_data(self.ValidationData[0])
		self.TestData[0] = generate_char_vector_data(self.TestData[0])

	def save(self):
		return [self.TrainData, self.ValidationData, self.TestData]


def generate_dataset(posts, link=False):
	random.shuffle(posts)

	data_x, data_y = [], []
	data_z = []

	for post in posts:
		data_x.append(text_preprocessing(post[6]))

		y = [0, 0, 0]
		y[post[2]] = 1
		data_y.append(y)

		if link:
			data_z.append(str(post[0]) + '_' + str(post[1]))

	return [data_x, data_y, data_z]


def create(size=30000) -> DataSet:
	size = size // 3

	query = "SELECT * FROM reviews WHERE type = {0} AND accepted = 1 ORDER BY RANDOM() LIMIT {1}"

	random_posts_2 = database.create_select_query(config.DATA_PATH + config.REVIEWS_DATABASE, query.format(2, size))
	random_posts_1 = database.create_select_query(config.DATA_PATH + config.REVIEWS_DATABASE, query.format(1, size))
	random_posts_0 = database.create_select_query(config.DATA_PATH + config.REVIEWS_DATABASE, query.format(0, size))

	parts = size // 100

	train_set = random_posts_0[0:parts * 80] + random_posts_1[0:parts * 80] + random_posts_2[0:parts * 80]
	validation_set = random_posts_0[parts * 80:parts * 85] + random_posts_1[parts * 80:parts * 85] + \
	                 random_posts_2[parts * 80:parts * 85]
	test_set = random_posts_0[parts * 85:parts * 100] + random_posts_1[parts * 85:parts * 100] + \
	           random_posts_2[parts * 85:parts * 100]

	train_set = generate_dataset(train_set)
	validation_set = generate_dataset(validation_set)
	test_set = generate_dataset(test_set, True)

	return DataSet([train_set, validation_set, test_set])

