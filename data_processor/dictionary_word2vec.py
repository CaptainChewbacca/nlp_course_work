import os
import gensim
import time

from database import database
from data_processor import dataset

class Sentences(object):
	def __init__(self, dirname):
		self.dirname = dirname

	def __iter__(self):
		for fname in os.listdir(self.dirname):
			for line in open(os.path.join(self.dirname, fname)):
				yield line.split()




def create_sentences(database_path_from, corpus_path_to):
	posts = database.create_select_query(database_path_from, "SELECT * FROM reviews")
	# file = open(database_path_to, mode='w')

	print("Start")
	start_time = time.time()
	sentences = []
	for post in posts:
		text = dataset.text_preprocessing(post[6])
		sentences.append(text)

	with open(corpus_path_to, 'w+', encoding='utf8') as file:
		for sentence in sentences:
			file.write(sentence)

	print("End ", time.time() - start_time)


def create(corpus_path_from, database_path_to):
	#data = gensim.models.word2vec.LineSentence(corpus_path_from)
	data = Sentences(corpus_path_from)

	print("data create")
	model = gensim.models.Word2Vec(data, size=300, window=10, min_count=5, sg=0)
	print("model create")

	model.init_sims(replace=True)

	model.save(database_path_to)


def load_model(database_path_to) -> gensim.models.Word2Vec:
	return gensim.models.Word2Vec.load(database_path_to)
