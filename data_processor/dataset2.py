import csv

import numpy as np
import config
from database import database



def text_preprocessing(text):
	delete_symbol = """(){}[]|&"""
	punctuation = """.,?!:;-"""
	text = text.lower().replace('\n', ' ')
	text = text.replace('|', '.')
	text = text.replace('<br />', ' ')
	for c in delete_symbol:
		text = text.replace(c, ' ')
	for c in punctuation:
		text = text.replace(c, ' ' + c + ' ')
	return text.split()

def generate_word_vector_data(data_x, words, size=200):
	out_data = []
	for x in data_x:
		out_data_x = []
		count = 0
		for word in x:
			if count < 500:
				try:
					out_data_x.append(words[word])
					count += 1
				except:
					pass
		#print(len(out_data_x))
		if count == 0:
			out_data_x.append(size)
		out_data.append(out_data_x)
	return out_data







def tokenize_from_dictionary(data_x, dictionary):
	pass


def generate_char_vector_data(data_x):
	char_dict = {
		' ': 0, '.': 1, ',': 2, '?': 3, '!': 4, ':': 5, ';': 6, '-': 7,
		'а': 10, 'б': 11, 'в': 12, 'г': 13, 'д': 14, 'е': 15, 'ё': 16, 'ж': 17, 'з': 18, 'и': 19, 'й': 20,
		'к': 21, 'л': 22, 'м': 23, 'н': 24, 'о': 25, 'п': 26, 'р': 27, 'с': 28, 'т': 29, 'у': 30, 'ф': 31,
		'х': 32, 'ц': 33, 'ч': 34, 'ш': 35, 'щ': 36, 'ъ': 37, 'ы': 38, 'ь': 39, 'э': 40, 'ю': 41, 'я': 42,

		'a': 43, 'b': 44, 'c': 45, 'd': 46, 'e': 47, 'f': 48, 'g': 49, 'h': 50, 'i': 51, 'j': 52, 'k': 53,
		'l': 54, 'm': 55, 'n': 56, 'o': 57, 'p': 58, 'q': 59, 'r': 60, 's': 61, 't': 62, 'u': 63, 'v': 64,
		'w': 65, 'x': 66, 'y': 67, 'z': 68,

		'0': 70, '1': 71, '2': 72, '3': 73, '4': 74, '5': 75, '6': 76, '7': 77, '8': 78, '9': 79,
	}
	out_data = []
	source_text = ' '.join(data_x)
	count = 0
	for char in source_text:
		if char in char_dict:
			out_data.append(char_dict[char])
			count += 1
	return np.array(out_data, dtype=int), count




def read_csv_dataset(file_name, set_type):
	import tensorflow as tf
	dataset = tf.data.TextLineDataset(config.DATA_PATH + 'dataset/' + file_name + '_{0}.csv'.format(set_type))
	#dataset = tf.data.TextLineDataset('C:/DATASET/' + file_name + '_{0}.csv'.format(set_type))
	return dataset


def read_csv_dataset_old(file_name, set_type):
	import csv
	with open(config.DATA_PATH + 'dataset/' + file_name + '_{0}.csv'.format(set_type)) as csvfile:
		reader = csv.reader(csvfile)
		for row in reader:
			yield row


def create_csv_data(file_name):

	from random import shuffle

	size = 64000

	query = "SELECT * FROM reviews WHERE type = {0} AND accepted = 1 ORDER BY RANDOM() LIMIT {1}"

	random_posts_2 = database.create_select_query(config.DATA_PATH + config.REVIEWS_DATABASE, query.format(2, size))
	random_posts_1 = [] #database.create_select_query(config.DATA_PATH + config.REVIEWS_DATABASE, query.format(1, size))
	random_posts_0 = database.create_select_query(config.DATA_PATH + config.REVIEWS_DATABASE, query.format(0, size))

	parts = size // 100

	train_set = random_posts_0[0:parts * 80] + random_posts_1[0:parts * 80] + random_posts_2[0:parts * 80]
	validation_set = random_posts_0[parts * 80:parts * 85] + random_posts_1[parts * 80:parts * 85] + \
	                 random_posts_2[parts * 80:parts * 85]
	test_set = random_posts_0[parts * 85:parts * 100] + random_posts_1[parts * 85:parts * 100] + \
	           random_posts_2[parts * 85:parts * 100]

	shuffle(train_set)
	shuffle(validation_set)
	shuffle(test_set)

	with open(config.DATA_PATH + 'dataset/' + file_name + '_train.csv', "w", newline="") as file:
		writer = csv.writer(file)
		writer.writerows(train_set)

	with open(config.DATA_PATH + 'dataset/' + file_name + '_valid.csv', "w", newline="") as file:
		writer = csv.writer(file)
		writer.writerows(validation_set)

	with open(config.DATA_PATH + 'dataset/' + file_name + '_test.csv', "w", newline="") as file:
		writer = csv.writer(file)
		writer.writerows(test_set)


