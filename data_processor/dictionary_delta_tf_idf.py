import pymorphy2
from database import database
import math
import re
import time


morph = pymorphy2.MorphAnalyzer()

def text_preprocessing(text):
    text = text.replace('!', ' ').replace('&', ' ').replace(';', ' ').replace(':', ' ').replace('?', ' ')\
    .replace(',', ' ').replace('.', ' ').replace('(', ' ').replace(')', ' ').replace('|', ' ').replace('-', ' ').lower()

    text = re.sub(r' не[ ]{0,5}', ' не', text)

    words = text.split()
    out = []
    for word in words:
        word = word.replace(' ', '')
        if len(word) > 1:
            norm_word = morph.parse(word)[0].normal_form
            out.append(norm_word)
    return out

#region database
def add_word(dictionary, word, type):
    if word in dictionary:
        if type == 0:
            dictionary[word][1] += 1
        elif type == 1:
            dictionary[word][0] += 1
            dictionary[word][1] += 1
        else:
            dictionary[word][0] += 1
    else:
        if type == 0:
            dictionary[word] = [0,1,0]
        elif type == 1:
            dictionary[word] = [1,1,0]
        else:
            dictionary[word] = [1,0,0]


def full_dictionary_from_database(database_path):
    words = database.create_select_query(database_path, "SELECT * FROM dictionary")
    out_dictionary = {}
    for i in range(len(words)):
        out_dictionary[words[i][0]] = [words[i][1],words[i][2],words[i][3], i]
    return out_dictionary

def dictionary_from_database(database_path):
    words = database.create_select_query(database_path,
            "SELECT * FROM dictionary WHERE (log >= 0.3 OR log <= -0.3) AND (negative + positive >= 300) ORDER BY log DESC;")
    out_dictionary = {}
    for i in range(len(words)):
        out_dictionary[words[i][0]] = [words[i][1],words[i][2],words[i][3], i]
    return out_dictionary

def compute_log(entry, count):
    pos = (entry[0] + 1)*count[1]
    neg = (entry[1] + 1)*count[0]
    return math.log(pos/neg)



def dictionary_to_database(dictionary, database_path):
    out = "INSERT OR REPLACE INTO dictionary (word,positive,negative,log) VALUES "
    count = dictionary['count']
    word_min_count = math.sqrt(count[0]+count[1])/10

    for k in dictionary.keys():
        if dictionary[k][0] + dictionary[k][1] > word_min_count or k == "count":
            log = compute_log(dictionary[k], count)
            if not -0.2 < log < 0.2 or k == "count":
                out += "('{0}',{1},{2},{3}),".format(k,
                                                 dictionary[k][0],
                                                 dictionary[k][1],
                                                 log)

    database.create_query(database_path, out[:-1])
#endregion


def create(database_path_from, database_path_to, n_gram=1):


    database.create_query(database_path_to,
                          """CREATE TABLE dictionary (
                            word  TEXT PRIMARY KEY
                                       UNIQUE
                                       NOT NULL,
                            positive      NOT NULL,
                            negative      NOT NULL,
                            log        NOT NULL
                            );"""
                          )

    dictionary = dictionary_from_database(database_path_to)

    posts = database.create_select_query(database_path_from, "SELECT * FROM reviews")

    print("Start")

    start_time = time.time()
    for post in posts:
        text = text_preprocessing(post[6])
        add_word(dictionary, "count", post[2])
        for i in range(len(text) - n_gram + 1):
            new_word = ""
            for j in range(n_gram):
                new_word += text[i+j] + ' '
            add_word(dictionary, new_word[:-1], post[2])

    print("End ", time.time() - start_time)


    print("Wirte to database")
    start_time = time.time()
    dictionary_to_database(dictionary, database_path_to)
    print("Write end ", time.time() - start_time)

