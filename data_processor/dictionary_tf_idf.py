import pymorphy2
from database import database
import math
import re
import time


morph = pymorphy2.MorphAnalyzer()


def text_preprocessing(text):
    text = text.replace('!', ' ').replace('&', ' ').replace(';', ' ').replace(':', ' ').replace('?', ' ')\
    .replace(',', ' ').replace('.', ' ').replace('(', ' ').replace(')', ' ').replace('|', ' ').replace('-', ' ').lower()

    text = re.sub(r' не[ ]{0,5}', ' не', text)

    words = text.split()
    out = []
    for word in words:
        word = word.replace(' ', '')
        if len(word) > 1:
            norm_word = morph.parse(word)[0].normal_form
            out.append(norm_word)
    return out

#region database

def add_word(dictionary, word, i=-1):
    if word in dictionary:
        if i != dictionary[word][1]:
            dictionary[word][0] += 1
            dictionary[word][1] = i
    else:
        dictionary[word] = [1, i]


def full_dictionary_from_database(database_path):
    words = database.create_select_query(database_path, "SELECT * FROM dictionary")
    out_dictionary = {}
    for i in range(len(words)):
        out_dictionary[words[i][0]] = [words[i][1],words[i][2], i]
    return out_dictionary


def dictionary_from_database(database_path):
    words = database.create_select_query(database_path, "SELECT * FROM dictionary WHERE (log <= 6.6) AND (count >= 15) ORDER BY log DESC;")
    out_dictionary = {}
    for i in range(len(words)):
        out_dictionary[words[i][0]] = [words[i][1], words[i][2], i]
    return out_dictionary


def compute_log(entry, count):
    return math.log(count[0]/entry[0])

def dictionary_to_database(dictionary, database_path):
    out = "INSERT OR REPLACE INTO dictionary (word,count,log) VALUES "
    for k in dictionary.keys():
        out += "('{0}',{1},{2}),".format(k, dictionary[k][0], compute_log(dictionary[k], dictionary['count']))
    database.create_query(database_path, out[:-1])

#endregion

def create(database_path_from, database_path_to):


    dictionary = dictionary_from_database(database_path_to)

    posts = database.create_select_query(database_path_from, "SELECT * FROM reviews ORDER BY RANDOM() LIMIT 50000")

    print("Start")

    start_time = time.time()

    i = 0
    for post in posts:
        i += 1

        text = text_preprocessing(post[6])
        add_word(dictionary, "count", i)
        for word in text:
            add_word(dictionary, word, i)


    print("End",time.time()-start_time)

    print("Wirte to database")
    start_time = time.time()
    dictionary_to_database(dictionary, database_path_to)
    print("Write end", time.time() - start_time)

