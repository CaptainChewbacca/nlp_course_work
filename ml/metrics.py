import numpy as np


def get_precision(confusion_matrix):
	sum = np.sum(confusion_matrix, axis=0)
	return (confusion_matrix[0][0]/sum[0],confusion_matrix[1][1]/sum[1],confusion_matrix[2][2]/sum[2])

def get_recall(confusion_matrix):
	sum = np.sum(confusion_matrix, axis=1)
	return (confusion_matrix[0][0]/sum[0],confusion_matrix[1][1]/sum[1],confusion_matrix[2][2]/sum[2])


def get_f1_score(confusion_matrix):
	p = get_precision(confusion_matrix)
	r = get_recall(confusion_matrix)
	out = []
	for i in range(len(p)):
		out.append(2*(p[i]*r[i])/(p[i]+r[i]))
	return out

def get_mean_precision(confusion_matrix):
	return np.trace(confusion_matrix)/np.sum(confusion_matrix)