import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn

from ml.models import model


class Model(model.Model):
	def __init__(self, name="grucnn3_model_1", load_old=False):
		tf.reset_default_graph()

		learning_rate = 0.001

		self.x = tf.placeholder(tf.float32, [None, 500, 200])
		self.y = tf.placeholder(tf.float32, [None, 3])
		self.length_x = tf.placeholder(tf.int32, [None])
		self.phase_train = tf.placeholder(tf.bool)
		self.keep_prob = tf.placeholder_with_default(tf.constant(1.0, dtype=tf.float32), ())


		with tf.variable_scope("rnn"):
			cells = self._create_layer(120, self.keep_prob)
			#cells = rnn.MultiRNNCell(cells=cells)
			outputs, states = tf.nn.dynamic_rnn(cells, self.x, dtype=tf.float32)

			lstm_output = outputs

		with tf.variable_scope("cnn_1"):
			w1, b1 = self._get_filter_WB(3, 120, 60, "1")
			#w2, b2 = self._get_filter_WB(3, 60, 50, "2")
			#w3, b3 = self._get_filter_WB(3, 50, 30, "3")

			conv_network = self._create_conv1d(lstm_output, w1, b1, self.keep_prob)
			#conv_network = self._create_conv1d(conv_network, w2, b2, self.keep_prob)
			#conv_network = self._create_conv1d(conv_network, w3, b3, self.keep_prob)
			conv_network = self._create_polling(conv_network, 500)

			conv_network1 = tf.reshape(conv_network, [-1, 60])

		with tf.variable_scope("cnn_2"):
			w1, b1 = self._get_filter_WB(4, 120, 60, "1")

			conv_network = self._create_conv1d(lstm_output, w1, b1, self.keep_prob)
			conv_network = self._create_polling(conv_network, 500)

			conv_network2 = tf.reshape(conv_network, [-1, 60])

		with tf.variable_scope("cnn_3"):
			w1, b1 = self._get_filter_WB(5, 120, 60, "1")

			conv_network = self._create_conv1d(lstm_output, w1, b1, self.keep_prob)
			conv_network = self._create_polling(conv_network, 500)

			conv_network3 = tf.reshape(conv_network, [-1, 60])


		with tf.variable_scope("logits"):

			conv_out = tf.concat([conv_network1, conv_network2, conv_network3], 1)
			conv_out = tf.nn.dropout(conv_out, self.keep_prob)

			w1, b1 = self._get_WB(180, 3, "out_1")
			logits = tf.add(tf.matmul(conv_out, w1), b1)






		with tf.variable_scope("loss"):
			self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=self.y))
			self.loss = self.loss + 0.001 * self._decay()

		with tf.variable_scope("optimizer"):
			self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(self.loss)


		self.pred = tf.argmax(logits, 1)
		self.correct_pred = tf.equal(self.pred, tf.argmax(self.y, 1))
		self.accuracy = tf.reduce_mean(tf.cast(self.correct_pred, tf.float32))

		model.Model.__init__(self, name, load_old)

		tf.summary.scalar("loss", self.loss)
		tf.summary.scalar("accuracy", self.accuracy)
		self.merged = tf.summary.merge_all()


	def _decay(self):
		costs = []
		for var in tf.trainable_variables():
			if var.op.name.find(r'W_') > 0:
				costs.append(tf.nn.l2_loss(var))
		return tf.add_n(costs)

	def _get_WB(self, input, output, layer_name):
		w = tf.get_variable("W_" + layer_name, shape=[input, output], initializer=tf.keras.initializers.he_normal())
		b = tf.get_variable("B_" + layer_name, shape=[output], initializer=tf.zeros_initializer())
		return w, b

	def _get_filter_WB(self, width, input, output, layer_name):
		w = tf.get_variable("W_"+layer_name, shape=[width, input, output],
		                    initializer=tf.truncated_normal_initializer(0.0, 2.0/(width*input)))
		b = tf.get_variable("B_"+layer_name, shape=[output], initializer=tf.zeros_initializer())
		return w, b

	def _create_layer(self, size, dropout):
		cell = rnn.GRUCell(size)
		#cell = rnn.DropoutWrapper(cell, input_keep_prob=dropout, state_keep_prob=dropout,
		#                                          output_keep_prob=dropout)
		return cell


	def _create_conv1d(self, tensor, w, b, keep_prob):
		tensor = tf.nn.conv1d(tensor, w, stride=1, padding='SAME')
		tensor = tf.nn.bias_add(tensor, b)
		#tensor = self._batch_norm(tensor)
		tensor = tf.nn.relu(tensor)
		return tf.nn.dropout(tensor, keep_prob)

	def _create_polling(self, tensor, size):
		#return tf.nn.max_pool(conv_network, ksize=[1, 1, size, 1], strides=[1, 1, size, 1], padding='SAME')
		return tf.nn.pool(tensor, window_shape=[size], strides=[size], padding='SAME', pooling_type="MAX")


	def _batch_norm(self, tensor, axis=[0, 1], scope='bn'):
		n_out = tensor.get_shape()[-1]
		with tf.variable_scope(scope):
			beta = tf.Variable(tf.constant(0.0, shape=[n_out]),
			                   name='beta', trainable=True)
			gamma = tf.Variable(tf.constant(1.0, shape=[n_out]),
			                    name='gamma', trainable=True)
			batch_mean, batch_var = tf.nn.moments(tensor, axis, name='moments')
			ema = tf.train.ExponentialMovingAverage(decay=0.5)

			def mean_var_with_update():
				ema_apply_op = ema.apply([batch_mean, batch_var])
				with tf.control_dependencies([ema_apply_op]):
					return tf.identity(batch_mean), tf.identity(batch_var)

			mean, var = tf.cond(self.phase_train,
			                    mean_var_with_update,
			                    lambda: (ema.average(batch_mean), ema.average(batch_var)))
			normed = tf.nn.batch_normalization(tensor, mean, var, beta, gamma, 1e-3)
		return normed




	def train(self, train_data, valid_data):

		def train_generator(train_data, i, batch_size):

			random_batch = np.random.randint(len(train_data[1]), size=batch_size)

			batch_xs = [train_data[0][i] for i in random_batch]
			batch_length_xs = [len(x) for x in batch_xs]
			batch_xs = [np.pad(x, ((0, 500 - len(x)), (0, 0)), 'constant') for x in batch_xs]

			batch_ys = [train_data[1][i] for i in random_batch]

			_, summary, pred = self.main_session.run([self.optimizer, self.merged, self.pred],
			                                         feed_dict={self.x: batch_xs, self.y: batch_ys,
			                                                    self.length_x: batch_length_xs,
			                                                    self.phase_train: True,
			                                                    self.keep_prob: 0.99})
			return summary, pred, batch_ys


		def valid_generator(valid_data, i, batch_size):
			batch_xs = valid_data[0][i * batch_size: i * batch_size + batch_size]
			batch_length_xs = [len(x) for x in batch_xs]
			batch_xs = [np.pad(x, ((0, 500 - len(x)), (0, 0)), 'constant') for x in batch_xs]

			batch_ys = valid_data[1][i * batch_size: i * batch_size + batch_size]

			accuracy, pred = self.main_session.run([self.accuracy, self.pred],
			                                       feed_dict={self.x: batch_xs, self.y: batch_ys,
			                                                  self.length_x: batch_length_xs,
			                                                  self.phase_train: False})
			return accuracy, pred, batch_ys


		self.__train_classifier(train_generator, train_data, valid_generator, valid_data,
		                        training_epochs=60, batch_size=50, training_cm_iter_size=10, epoch_iterations=100)


	def test(self, test_data):
		def test_generator(test_data, i, batch_size):
			batch_xs = test_data[0][i * batch_size: i * batch_size + batch_size]
			batch_length_xs = [len(x) for x in batch_xs]

			batch_xs = [np.pad(x, ((0, 500 - len(x)), (0, 0)), 'constant') for x in batch_xs]
			pred = self.main_session.run(self.pred, feed_dict={self.x: batch_xs, self.length_x: batch_length_xs,
			                                                        self.phase_train: False})
			return pred
		self.__test_classifier(test_generator, test_data, batch_size=100)


	def run(self, data_x):
		out = self.main_session.run(self.pred, feed_dict={self.x: data_x, self.phase_train: False})
		return out
