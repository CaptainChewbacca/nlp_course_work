import numpy as np
import tensorflow as tf
import math

from ml.models import model


class Model(model.Model):
	def __init__(self, name="multilayer_perceptron_model", load_old=False):
		tf.reset_default_graph()

		learning_rate = 0.0001

		self.x = tf.placeholder(tf.float32, [None, 300])
		self.y = tf.placeholder(tf.float32, [None, 3])


		def get_WB(input, output):
			stddev = tf.sqrt(2 / (input + output))
			w = tf.Variable(tf.random_normal([input, output], stddev=stddev))
			b = tf.Variable(tf.random_normal([output], stddev=stddev))
			return w, b

		self.W = []
		self.B = []

		w,b = get_WB(300, 200)
		self.W.append(w)
		self.B.append(b)

		w,b = get_WB(200, 100)
		self.W.append(w)
		self.B.append(b)

		w,b = get_WB(100, 20)
		self.W.append(w)
		self.B.append(b)

		w, b = get_WB(20, 3)
		self.W.append(w)
		self.B.append(b)


		def neural_net(x):

			layer_n = x
			M = len(self.W)
			for i in range(M - 1):
				layer_n = tf.add(tf.matmul(layer_n, self.W[i]), self.B[i])
				layer_n = tf.nn.relu(layer_n)

			out_layer = tf.matmul(layer_n, self.W[M - 1]) + self.B[M - 1]
			return out_layer


		logits = neural_net(self.x)

		self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=self.y))
		self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(self.loss)

		self.pred = tf.argmax(logits, 1)
		self.correct_pred = tf.equal(self.pred, tf.argmax(self.y, 1))
		self.accuracy = tf.reduce_mean(tf.cast(self.correct_pred, tf.float32))


		model.Model.__init__(self, name, load_old)


	def train(self, train_data, validation_data):
		training_epochs = 40
		batch_size = 1000

		for epoch in range(training_epochs):
			train_accuracy_sum = 0.
			total_batch = int(len(train_data[1]) / batch_size)
			for i in range(total_batch):
				batch_xs = train_data[0][i * batch_size: i * batch_size + batch_size]
				batch_ys = train_data[1][i * batch_size: i * batch_size + batch_size]

				_,train_accuracy,loss = self.main_session.run([self.optimizer,self.accuracy,self.loss],
				                                         feed_dict={self.x: batch_xs, self.y: batch_ys})
				train_accuracy_sum += train_accuracy/total_batch
				print(loss)

			validation_accuracy = self.main_session.run(self.accuracy,
			                                            feed_dict={self.x: validation_data[0], self.y: validation_data[1]})

			print("Epoch:", '%03d' % (epoch + 1), "v_acc=", "{:.3f}".format(validation_accuracy),
			                                      "t_acc=", "{:.3f}".format(train_accuracy_sum))

		print("Optimization Finished!")

		self.save()

	def test(self, test_data):
		confusion_matrix = np.zeros(shape=(3, 3), dtype=int)
		out = self.main_session.run(self.pred, feed_dict={self.x: test_data[0]})

		for i in range(len(test_data[1])):
			if test_data[1][i].index(1) != out[i]:
				str = test_data[2][i].split('_')
				str = "https://www.kinopoisk.ru/user/"+str[0]+"/comment/"+str[1]+"/"
				print(test_data[1][i].index(1),' ', out[i], ' : ', str)

			confusion_matrix[test_data[1][i].index(1)][out[i]] += 1
		return confusion_matrix

	def run(self, data_x):
		out = self.main_session.run(self.pred, feed_dict={self.x: data_x})
		return out

