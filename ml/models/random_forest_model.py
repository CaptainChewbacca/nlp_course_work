import numpy as np
import tensorflow as tf
from tensorflow.contrib.tensor_forest.python import tensor_forest

from ml.models import model


class Model(model.Model):
	def __init__(self, name="random_forest_model", load_old=False):
		tf.reset_default_graph()

		num_classes = 3  # The 10 digits
		num_features = 12179  # Each image is 28x28 pixels
		num_trees = 20
		max_nodes = 10000

		self.x = tf.placeholder(tf.float32, [None, 12179])
		self.y = tf.placeholder(tf.float32, [None, 3])

		self.hparams = tensor_forest.ForestHParams(num_classes=num_classes,
		                                      num_features=num_features,
		                                      num_trees=num_trees,
		                                      max_nodes=max_nodes).fill()

		forest_graph = tensor_forest.RandomForestGraphs(self.hparams)
		# Get training graph and loss
		self.train_op = forest_graph.training_graph(self.x, tf.argmax(self.y, 1))
		self.loss_op = forest_graph.training_loss(self.x, tf.argmax(self.y, 1))

		self.pred = tf.argmax(forest_graph.inference_graph(self.x), 1)
		self.correct_pred = tf.equal(self.pred, tf.argmax(self.y, 1))
		self.accuracy = tf.reduce_mean(tf.cast(self.correct_pred, tf.float32))


		model.Model.__init__(self, name, load_old)


	def train(self, train_data_x, train_data_y):
		num_steps = 5
		batch_size = 500

		for epoch in range(0, num_steps):
			avg_cost = 0.
			total_batch = int(len(train_data_y) / batch_size)

			for i in range(total_batch):
				batch_xs = train_data_x[i * batch_size: i * batch_size + batch_size]
				batch_ys = train_data_y[i * batch_size: i * batch_size + batch_size]

				_, l = self.main_session.run([self.train_op, self.loss_op], feed_dict={self.x: batch_xs, self.y: batch_ys})
				acc = self.main_session.run(self.accuracy, feed_dict={self.x: batch_xs, self.y: batch_ys})
				avg_cost += acc / total_batch

			print("Epoch:", '%04d' % (epoch + 1), "cost=", "{:.9f}".format(avg_cost))

		print("Optimization Finished!")
		self.save()

	def test(self, eval_data_x, eval_data_y, eval_data_z):
		confusion_matrix = np.zeros(shape=(3, 3), dtype=int)
		out = self.main_session.run(self.pred, feed_dict={self.x: eval_data_x})

		for i in range(len(eval_data_y)):

			if eval_data_y[i].index(1) != out[i]:
				str = eval_data_z[i].split('_')
				str = "https://www.kinopoisk.ru/user/"+str[0]+"/comment/"+str[1]+"/"
				print(eval_data_y[i].index(1),' ', out[i], ' : ', str)

			confusion_matrix[eval_data_y[i].index(1)][out[i]] += 1
		return confusion_matrix

	def run(self, eval_data_x):
		out = self.main_session.run(self.pred, feed_dict={self.x: eval_data_x})
		return out

