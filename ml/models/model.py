import logging

import numpy as np

import tensorflow as tf
import config

from data_drawer import tensorboard_wrapper



class Model:

	def __init__(self, name, load_old):
		self.name = name
		self.path = config.DATA_PATH + "models/" + name + "/model"
		self.saver = tf.train.Saver()
		self.main_session = tf.Session()
		init = tf.global_variables_initializer()
		self.main_session.run(init)

		if load_old:
			self.load()


	def load(self):
		if self.path:
			try:
				self.saver.restore(self.main_session, self.path)
			except Exception as ex:
				print("Ошибка при загрузке модели (Не существует)")

	def save(self):
		if self.path:
			self.saver.save(self.main_session, self.path)




	def __train_classifier(self, train_generator, train_data,
			                     valid_generator, valid_data,
			                     training_epochs=20, batch_size=500, training_cm_iter_size=10, epoch_iterations=-1):

		train_writer = tf.summary.FileWriter(config.TF_LOG_DIR.format(self.name) + '/train', graph=tf.get_default_graph())
		valid_writer = tf.summary.FileWriter(config.TF_LOG_DIR.format(self.name) + '/valid', graph=tf.get_default_graph())

		logging.info("Start train with: {0} training epochs, {1} batch_size".format(training_epochs, batch_size))

		last_valid_accuracy = 0

		for epoch in range(training_epochs):

			if epoch_iterations > 0:
				total_batch = epoch_iterations
			else:
				total_batch = int(len(train_data[1]) / batch_size)

			confusion_matrix = np.zeros(shape=(3, 3), dtype=int)
			for step in range(total_batch):
				summary, pred, true_label = train_generator(train_data, step, batch_size)
				train_writer.add_summary(summary, epoch * total_batch + step)


				for i in range(len(true_label)):
					confusion_matrix[true_label[i].index(1)][pred[i]] += 1

				if (step % training_cm_iter_size) == 0:
					img_d_summary = tensorboard_wrapper.plot_confusion_matrix(confusion_matrix,
					                                                          ["Отриц.", "Нейтр.", "Полож."],
					                                                          tensor_name='dev/cm')
					train_writer.add_summary(img_d_summary, epoch * total_batch + step)
					confusion_matrix.fill(0)

				logging.info("Epoch: {0}, step: {1}".format(epoch+1, step+1))


# Test phase
# ---//---//---//---//---//---//---//---//---//---//---//---//---//---//---//---//---//---//---
			total_batch = int(len(valid_data[1]) / batch_size)

			accuracies = []
			confusion_matrix = np.zeros(shape=(3, 3), dtype=int)

			for step in range(total_batch):
				accuracy, pred, true_label = valid_generator(valid_data, step, batch_size)
				accuracies.append(accuracy)

				for i in range(len(true_label)):
					confusion_matrix[true_label[i].index(1)][pred[i]] += 1

			img_d_summary = tensorboard_wrapper.plot_confusion_matrix(confusion_matrix,
			                                                          ["Отриц.", "Нейтр.", "Полож."],
			                                                          tensor_name='dev/cm')
			valid_writer.add_summary(img_d_summary, epoch)
			accuracy = np.mean(accuracies)
			summary = tf.Summary()
			summary.value.add(tag="accuracy_validation", simple_value=accuracy)
			valid_writer.add_summary(summary, epoch)

			logging.info("Validation accuracy: {0}".format(accuracy))

			if accuracy >= last_valid_accuracy:
				self.save()
				logging.info("Model saved, accuracy increased by: {0}".format(accuracy - last_valid_accuracy))
				last_valid_accuracy = accuracy

		logging.info("Train finish!")





	def __test_classifier(self, test_generator, test_data, batch_size=500):
		test_writer = tf.summary.FileWriter(config.TF_LOG_DIR.format(self.name) + '/test', graph=tf.get_default_graph())

		logging.info("Start test with: {0} batch_size".format(batch_size))

		confusion_matrix = np.zeros(shape=(3, 3), dtype=int)
		total_batch = int(len(test_data[1]) / batch_size)
		out = []
		for step in range(total_batch):
			out += list(test_generator(test_data, step, batch_size))

		file_out = open("test_out.txt", "+a")

		for i in range(len(test_data[1])):
			"""if test_data[1][i].index(1) != out[i]:
				str = test_data[2][i].split('_')
				str = "https://www.kinopoisk.ru/user/" + str[0] + "/comment/" + str[1] + "/"
				print(test_data[1][i].index(1), ' ', out[i], ' : ', str)"""
			confusion_matrix[test_data[1][i].index(1)][out[i]] += 1
			file_out.write(str(out[i])+'\n')

		file_out.close()

		img_d_summary = tensorboard_wrapper.plot_confusion_matrix(confusion_matrix,
		                                                          ["Отриц.", "Нейтр.", "Полож."],
		                                                          tensor_name='dev/cm')
		test_writer.add_summary(img_d_summary, 1)

		logging.info("Test finish!")