import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn

from ml.models import model


class Model(model.Model):
	def __init__(self, name="charbychar_model", load_old=False):
		tf.reset_default_graph()

		learning_rate = 0.001
		dropout_rate = 0.5

		self.x = tf.placeholder(tf.int32, [None, 2000])
		self.y = tf.placeholder(tf.int32, [None, 3])
		self.length_x = tf.placeholder(tf.int32, [None])
		self.phase_train = tf.placeholder(tf.bool)
		self.input_keep_prob = tf.placeholder_with_default(tf.constant(1.0, dtype=tf.float32), ())
		self.output_keep_prob = tf.placeholder_with_default(tf.constant(1.0, dtype=tf.float32), ())
		self.state_keep_prob = tf.placeholder_with_default(tf.constant(1.0, dtype=tf.float32), ())


		self.x = tf.placeholder(tf.int32, [None, 2000])
		self.embedding_encoder = tf.get_variable("embedding_encoder", shape=[80, 10],
		                                         initializer=tf.random_uniform_initializer(-1, 1), trainable=True)
		self.emb_input = tf.nn.embedding_lookup(self.embedding_encoder, self.x)


		def get_WB(input, output, layer_name):
			w = tf.get_variable("W_" + layer_name, shape=[input, output], initializer=tf.keras.initializers.he_normal())
			b = tf.get_variable("B_" + layer_name, shape=[output], initializer=tf.zeros_initializer())
			return w, b

		def create_layer(size):
			cell = rnn.GRUCell(size)
			cell = rnn.DropoutWrapper(cell, input_keep_prob=self.input_keep_prob, state_keep_prob=self.state_keep_prob,
			                                          output_keep_prob=self.output_keep_prob)
			return cell


		with tf.variable_scope("rnn"):
			w1, b1 = get_WB(80, 3, 'out_1')

			gru_cell = [create_layer(130), create_layer(80)]
			gru_cell = rnn.MultiRNNCell(cells=gru_cell)

			outputs, states = tf.nn.dynamic_rnn(gru_cell, self.emb_input, dtype=tf.float32, sequence_length=self.length_x)
			states = states[1]
			# states = tf.layers.dropout(states, dropout_rate, training=self.phase_train)
			network = tf.add(tf.matmul(states, w1), b1)
			logits = network


		with tf.variable_scope("loss"):
			self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=self.y))
			self.regularizers = tf.nn.l2_loss(w1)
			self.loss = self.loss + 0.001 * self.regularizers

		with tf.variable_scope("optimizer"):
			self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(self.loss)


		self.pred = tf.argmax(logits, 1)
		self.correct_pred = tf.equal(self.pred, tf.argmax(self.y, 1))
		self.accuracy = tf.reduce_mean(tf.cast(self.correct_pred, tf.float32))


		model.Model.__init__(self, name, load_old)


		tf.summary.scalar("loss", self.loss)
		tf.summary.scalar("accuracy", self.accuracy)
		self.merged = tf.summary.merge_all()


	def train(self, train_data, valid_data):

		def train_generator(train_data, i, batch_size):

			random_batch = np.random.randint(len(train_data[1]), size=batch_size)

			batch_xs = [train_data[0][i] for i in random_batch]
			batch_length_xs = [len(x) for x in batch_xs]

			batch_xs = [np.pad(x, (0, 2000 - len(x)), 'constant') for x in batch_xs]

			batch_ys = [train_data[1][i] for i in random_batch]

			_, summary, pred = self.main_session.run([self.optimizer, self.merged, self.pred],
			                                         feed_dict={self.x: batch_xs, self.y: batch_ys,
			                                                    self.length_x: batch_length_xs,
			                                                    self.phase_train: True,
			                                                    self.input_keep_prob: 0.95,
			                                                    self.state_keep_prob: 0.95,
			                                                    self.output_keep_prob: 0.8})
			return summary, pred, batch_ys


		def valid_generator(valid_data, i, batch_size):
			batch_xs = valid_data[0][i * batch_size: i * batch_size + batch_size]
			batch_length_xs = [len(x) for x in batch_xs]
			batch_xs = [np.pad(x, (0, 2000 - len(x)), 'constant') for x in batch_xs]

			batch_ys = valid_data[1][i * batch_size: i * batch_size + batch_size]

			accuracy, pred = self.main_session.run([self.accuracy, self.pred],
			                                       feed_dict={self.x: batch_xs, self.y: batch_ys,
			                                                  self.length_x: batch_length_xs,
			                                                  self.phase_train: False})
			return accuracy, pred, batch_ys


		self.__train_classifier(train_generator, train_data, valid_generator, valid_data,
		                        training_epochs=20, batch_size=10, training_cm_iter_size=10)


	def test(self, test_data):
		def test_generator(test_data, i, batch_size):
			batch_xs = test_data[0][i * batch_size: i * batch_size + batch_size]
			batch_length_xs = [len(x) for x in batch_xs]

			batch_xs = [np.pad(x, (0, 2000 - len(x)), 'constant') for x in batch_xs]
			pred = self.main_session.run(self.pred, feed_dict={self.x: batch_xs, self.length_x: batch_length_xs,
			                                                        self.phase_train: False})
			return pred
		self.__test_classifier(test_generator, test_data, batch_size=10)


	def run(self, data_x):
		out = self.main_session.run(self.pred, feed_dict={self.x: data_x, self.phase_train: False})
		return out
