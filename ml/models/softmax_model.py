import numpy as np
import tensorflow as tf

from ml.models import model


def _get_streaming_metrics(prediction, label, num_classes):
	with tf.name_scope("test"):
		# the streaming accuracy (lookup and update tensors)
		accuracy, accuracy_update = tf.metrics.accuracy(label, prediction,
		                                                name='accuracy')
		# Compute a per-batch confusion
		batch_confusion = tf.confusion_matrix(label, prediction,
		                                      num_classes=num_classes,
		                                      name='batch_confusion')
		# Create an accumulator variable to hold the counts
		confusion = tf.Variable(tf.zeros([num_classes, num_classes],
		                                 dtype=tf.int32),
		                        name='confusion')
		# Create the update op for doing a "+=" accumulation on the batch
		confusion_update = confusion.assign(confusion + batch_confusion)
		# Cast counts to float so tf.summary.image renormalizes to [0,255]
		confusion_image = tf.reshape(tf.cast(confusion, tf.float32),
		                             [1, num_classes, num_classes, 1])
		# Combine streaming accuracy and confusion matrix updates in one op
		test_op = tf.group(accuracy_update, confusion_update)

		tf.summary.image('confusion', confusion_image)
		tf.summary.scalar('accuracy', accuracy)

	return test_op, accuracy, confusion


class Model(model.Model):
	def __init__(self, name="softmax_model", load_old=False):
		tf.reset_default_graph()

		learning_rate = 0.05

		with tf.name_scope('input'):
			self.x = tf.placeholder(tf.float32, [None, 21990])
			self.y = tf.placeholder(tf.float32, [None, 3])

		self.W = tf.Variable(tf.zeros([21990, 3]), name="W")
		self.b = tf.Variable(tf.zeros([3]), name="B")

		self.pred = tf.nn.softmax(tf.matmul(self.x, self.W) + self.b)

		self.loss = tf.reduce_mean(-tf.reduce_sum(self.y * tf.log(self.pred), reduction_indices=1))
		self.optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(self.loss)

		self.correct_pred = tf.equal(tf.argmax(self.pred, 1), tf.argmax(self.y, 1))
		self.accuracy = tf.reduce_mean(tf.cast(self.correct_pred, tf.float32))


		model.Model.__init__(self, name, load_old)

	def train(self, train_data, validation_data):
		init = tf.global_variables_initializer()

		training_epochs = 20
		batch_size = 1000

		self.main_session.run(init)

		for epoch in range(training_epochs):
			train_accuracy_sum = 0.
			total_batch = int(len(train_data[1]) / batch_size)

			for i in range(total_batch):
				batch_xs = train_data[0][i * batch_size: i * batch_size + batch_size]
				batch_ys = train_data[1][i * batch_size: i * batch_size + batch_size]

				_,train_accuracy,loss = self.main_session.run([self.optimizer,self.accuracy,self.loss],
				                                    feed_dict={self.x: batch_xs, self.y: batch_ys})
				train_accuracy_sum += train_accuracy / total_batch
				#print(loss)

			validation_accuracy = self.main_session.run(self.accuracy,
				                                    feed_dict={self.x: validation_data[0], self.y: validation_data[1]})

			print("Epoch:", '%03d' % (epoch + 1), "v_acc=", "{:.3f}".format(validation_accuracy),
			                                      "t_acc=", "{:.3f}".format(train_accuracy_sum))

		print("Optimization Finished!")

		self.save()

	def test(self, test_data):
		confusion_matrix = np.zeros(shape=(3, 3), dtype=int)
		out = self.main_session.run(tf.argmax(self.pred, 1), feed_dict={self.x: test_data[0]})

		for i in range(len(test_data[1])):
			if test_data[1][i].index(1) != out[i]:
				str = test_data[2][i].split('_')
				str = "https://www.kinopoisk.ru/user/"+str[0]+"/comment/"+str[1]+"/"
				print(test_data[1][i].index(1),' ', out[i], ' : ', str)

			confusion_matrix[test_data[1][i].index(1)][out[i]] += 1
		return confusion_matrix

	def run(self, eval_data_x):
		out = self.main_session.run(tf.argmax(self.pred, 1), feed_dict={self.x: eval_data_x})
		return out
