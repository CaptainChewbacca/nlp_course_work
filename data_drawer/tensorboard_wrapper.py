
import itertools
import matplotlib.pyplot as plt
import numpy as np

import tfplot
from matplotlib.figure import Figure
from ml2 import metrics


def plot_confusion_matrix(cm, classes, title='Confusion matrix', tensor_name = 'MyFigure/image', normalize=False):
	cm = np.array(cm)

	if normalize:
		cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

	np.set_printoptions(precision=2)

	fig = Figure(figsize=(4, 4), facecolor='w', edgecolor='k')

	ax = fig.add_subplot(2, 1, 1)
	ax.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)

	tick_marks = np.arange(len(classes))

	ax.set_xlabel('Предсказанные', fontsize=10)
	ax.set_xticks(tick_marks)
	ax.set_xticklabels(classes, fontsize=8, rotation=-45, ha='center')
	ax.xaxis.set_label_position('bottom')
	ax.xaxis.tick_bottom()

	ax.set_ylabel('Истинные', fontsize=10)
	ax.set_yticks(tick_marks)
	ax.set_yticklabels(classes, fontsize=8, rotation=-45, va='center')
	ax.yaxis.set_label_position('left')
	ax.yaxis.tick_left()

	fmt = '.2f' if normalize else 'd'
	thresh = cm.max() / 2.
	for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
		ax.text(j, i, format(cm[i][j], fmt),
		        horizontalalignment="center", fontsize=12, verticalalignment='center',
		        color="white" if cm[i][j] > thresh else "black")

	precision = metrics.get_precision(cm)
	recall = metrics.get_recall(cm)
	f1_score = metrics.get_f1_score(cm)
	mean_precision = metrics.get_mean_precision(cm)
	accuracy = metrics.get_accuracy(cm)

	precision = ['%.2f' % elem for elem in precision]
	recall = ['%.2f' % elem for elem in recall]
	f1_score = ['%.2f' % elem for elem in f1_score]
	mean_precision = '%.2f' % mean_precision
	accuracy = '%.2f' % accuracy

	ay = fig.add_subplot(2, 1, 2)
	ay.axes.get_xaxis().set_visible(False)
	ay.axes.get_yaxis().set_visible(False)
	"""ay.text(1, 8, "Precision: {0}".format(precision), fontsize=10)
	ay.text(1, 6, "Recall: {0}".format(recall), fontsize=10)
	ay.text(1, 4, "F1 score: {0}".format(f1_score), fontsize=10)
	ay.text(1, 2, "Mean precision: {0}".format(mean_precision), fontsize=10)"""

	ay.axis('tight')
	ay.axis('off')
	table = ay.table(cellText=[
		('Precision', ", ".join(precision)),
		('Recall', ", ".join(recall)),
		('F1 score', ", ".join(f1_score)),
		('Accuracy', str(accuracy))
	], colLabels=("Метрика", "Значения"), rowLoc='center', cellLoc='center', loc='center', fontsize=10, clip_on=True)
	table.scale(1, 1.6)
	ay.axis([0, 10, 0, 10])

	fig.set_tight_layout(True)

	summary = tfplot.figure.to_summary(fig, tag=tensor_name)
	return summary