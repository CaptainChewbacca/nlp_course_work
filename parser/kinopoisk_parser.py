from lxml import html
import threading
import re
import time
import requests
from multiprocessing.dummy import Pool as ThreadPool
import sqlite3


lock = threading.Lock()
def create_query(sql_text):
	lock.acquire()
	with sqlite3.connect('E:\Dataset_CourseWork\out_base.db', timeout=10) as con:
		cursor = con.cursor()
		cursor.execute(sql_text)
		con.commit()
	lock.release()

def get_reviews_count():
	with sqlite3.connect('E:\Dataset_CourseWork\out_base.db', timeout=10) as con:
		cursor = con.cursor()
		cursor.execute("SELECT Count(*) FROM reviews")
		return cursor.fetchall()[0][0]

def add_new_review(added):
	for i in range(0, len(added)):
		s = added[i][:-1]
		if s != '':
			query = "INSERT OR IGNORE INTO reviews (user_id,comment_id,type,accepted,date,rating,comment_text) VALUES {0} ".format(s)
			create_query(query)

def clear_comment(comment):
	out = comment.replace('<b>', '').replace('</b>', '').replace('<br />', '|') \
		.replace('&nbsp;', ' ').replace('&laquo;', '').replace('&raquo;', '')\
		.replace('&#151;', '-').replace('&#133;', ' ').replace('<i>','').replace('</i>','')
	reg = re.compile('[^a-zA-Zа-яёА-ЯЁ0-9!&;:?,.)(\|\- ]')
	return reg.sub('', out)

added_reviews = 0

def Get_Page_Comments(user_id, text):
	global added_reviews
	pos_a = 0
	pos_b = 0
	pos_c = 0
	pos_d = 0
	pos_e = 0
	pos_f = 0

	test_a = '<span class="_reachbanner_" itemprop="reviewBody">'
	test_d = """</span>
    
    <b class=\""""
	test_f = "<li id=\"comment_num_vote_"

	len_test_a = len(test_a)
	add = "({0},{1},{2},{3},'{4}','{5}','{6}'),"
	adding_query = ""
	while pos_a - len_test_a != -1:
		pos_f = text.find(test_f, pos_f) + 25
		pos_e = text.find('<span class="date">', pos_e) + 19
		pos_d = text.find(test_d, pos_d) + 27
		pos_c = text.find('id="div_review_', pos_c) + 15
		pos_b = text.find('class="response', pos_b) + 16
		pos_a = text.find(test_a, pos_a) + len_test_a

		if pos_a - len_test_a != -1:
			comment_text = clear_comment(text[pos_a: text.find('</span>', pos_a)])

			if text[pos_b] == "g":
				type = 2
			elif text[pos_b] == "b":
				type = 0
			else:
				type = 1

			comment_id = text[pos_c: text.find('"', pos_c)]

			accepted = 1 if text[pos_d] == 'g' else 0
			date = text[pos_e: text.find('</span>', pos_e)]

			pos_f = text.find('">', pos_f) + 2
			rating = text[pos_f: text.find('<', pos_f)]

			added_reviews += 1
			adding_query += add.format(user_id, comment_id, type, accepted, date, rating, comment_text)

	return adding_query


def Get_All_Page(user_id):
	pages = 1
	i = 1
	requests.get('https://www.kinopoisk.ru/', proxies = {"https":"socks4://81.28.166.37:1080"})
	url = 'https://www.kinopoisk.ru/user/{0}/comments/list/ord/date/page/{1}/perpage/200/#list'
	reviews = []
	while i <= pages:
		while True:
			try:
				r = requests.get(url.format(user_id, i), timeout=15, proxies = {"https":"socks4://81.28.166.37:1080"})
				break
			except Exception as ex:
				print("{0} : {1}".format(user_id, ex))

		res = r.text
		tree = html.fromstring(res)
		token = tree.xpath('//*[@id="list"]/h1')

		if len(token) > 0:
			pages = token[0].text
			pages = round(int(pages[pages.rfind('(') + 1:-1]) // 200 + 1)
			reviews.append(Get_Page_Comments(user_id, res))
		i += 1

	if reviews:
		add_new_review(reviews)


#534k now
for k in range(534, 535):
	print("Step_____%d"%k)
	print(" Begin count: %d"%get_reviews_count())

	start_time = time.time()
	added_reviews = 0
	pool = ThreadPool(1000)

	results = pool.map(Get_All_Page, range(k*1000, k*1000+1000))

	pool.close()
	pool.join()

	print(" End count: %d"%get_reviews_count())

	print(' now added: {0}'.format(added_reviews))
	print(time.time() - start_time)
